import React from "react";
import {View,Text,TouchableOpacity,Image, ImageBackground,TextInput, Alert} from "react-native";
import {Container, Header, Content, Card, CardItem, Body,Input, Button,Icon,Right,Item,Left,Thumbnail} from "native-base";
import {responsiveHeight,responsiveWidth,responsiveFontSize} from "react-native-responsive-dimensions";
import { LinearGradient } from "expo-linear-gradient";
import HeaderCOM from '../../../../components/Header';
import {SocialIcon} from 'react-native-elements';
import { SafeAreaView } from "react-native-safe-area-context";

export default class About extends React.Component{
render(){
return(
    <SafeAreaView style={{flex:1}}>
        <View style={{flex:0.1, flexDirection:"row", backgroundColor:"#f11", alignItems:"center"}}>
            <TouchableOpacity style={{marginLeft:10}}>
                <Icon name="menu" type="Feather" style={{color:"white", marginTop:5}}/>
            </TouchableOpacity>
            <Text style={{color:"white",fontSize:responsiveFontSize(3), marginLeft:10}}>About Us</Text>
        </View>
        <LinearGradient start={{x: 0, y: 0}} end={{x: 0, y: 1}} colors={['#6B0101', '#FF0000']} style={{flex:1}}>
            {/* <HeaderCOM {...this.props} 
                            name="About Us" 
                            ltbtnValue={true} 
                            rtfBtnValue={true} 
                            ltbtnTitle="arrow-back" 
                            rtbtnTitle="dots-three-vertical" 
                            onActionLtBtn = {()=>{this.props.navigation.goBack()}}
                            /> */}
            <Content padder>
                <View style={{backgroundColor:"#bfbfbf",width:responsiveWidth(60),height:responsiveWidth(60),margin:responsiveWidth(20),
                                borderRadius:responsiveWidth(30),marginTop:responsiveHeight(5),marginBottom:responsiveHeight(3),
                                alignItems:"center",justifyContent:"center"}}>
                    <View style={{backgroundColor:"#b3b3b3",width:responsiveWidth(50),height:responsiveWidth(50),
                                borderRadius:responsiveWidth(30),alignItems:"center",justifyContent:"center"}}>
                        <View style={{backgroundColor:"#a6a6a6",width:responsiveWidth(40),height:responsiveWidth(40),
                                borderRadius:responsiveWidth(20),alignItems:"center",justifyContent:"center"}}>
                            <View style={{backgroundColor:"#fff",width:responsiveWidth(30),height:responsiveWidth(30),
                                borderRadius:responsiveWidth(15),alignItems:"center",justifyContent:"center"}}>
                                    <Image source={require("../../../../assets/uss.jpg")} 
                                            style={{width:responsiveWidth(30),height:responsiveWidth(30),borderRadius:responsiveWidth(15)}}/>
                            </View>
                        </View>
                    </View>
                </View>
                <View style={{justifyContent:"center",alignItems:"center"}}>
                    <Item >
                        <Text style={{color:"#fff",fontSize:responsiveFontSize(5),fontWeight:"bold"}}>
                            Nomad
                        </Text>
                    </Item>
                    <Text style={{color:"#fff",fontWeight:"bold",fontSize:responsiveFontSize(3),marginTop:responsiveHeight(5)}}>
                        You have every right to a
                    </Text>
                    <Text style={{color:"#fff",fontWeight:"bold",fontSize:responsiveFontSize(3)}}>
                    convenient life
                    </Text>
                </View>
                <View style={{flexDirection:"row",justifyContent:"center",marginTop:responsiveHeight(5)}}>
                    <TouchableOpacity style={{alignItems:"center",justifyContent:"center"}}> 
                            <SocialIcon type="twitter"/>
                    </TouchableOpacity>
                    <TouchableOpacity style={{alignItems:"center",justifyContent:"center"}}> 
                            <SocialIcon type="facebook"/>
                    </TouchableOpacity>
                    <TouchableOpacity style={{alignItems:"center",justifyContent:"center"}}> 
                            <SocialIcon type="google-plus-official" />
                    </TouchableOpacity>
                    <TouchableOpacity style={{alignItems:"center",justifyContent:"center"}}> 
                            <SocialIcon type="linkedin"/>
                    </TouchableOpacity>
                </View>
                <Card style={{backgroundColor:"#f2f2f2",borderRadius:responsiveWidth(5),marginTop:responsiveHeight(5)}}>
                    <CardItem style={{backgroundColor:"#d9d9d9",borderRadius:responsiveWidth(5)}}>
                        <Text style={{fontWeight:"bold",fontSize:responsiveFontSize(2.5)}}>APP</Text>
                    </CardItem>
                    <CardItem bordered style={{backgroundColor:"#f2f2f2",borderRadius:responsiveWidth(5)}}>
                        <TouchableOpacity  onPress={()=>{this.props.navigation.navigate("support")}} style={{flexDirection:"row"}}>
                            <Left>
                                <Thumbnail source={require("../../../../assets/icon/help.jpg")} 
                                        style={{height:responsiveWidth(10),width:responsiveWidth(10)}}/>
                                <Text style={{fontWeight:"400",marginLeft:responsiveWidth(3),fontSize:responsiveFontSize(2)}}>
                                    Help</Text>
                            </Left>
                            <Right>
                                <Icon name="ios-arrow-forward" type="Ionicons"/>
                            </Right>
                        </TouchableOpacity>
                    </CardItem>
                    <CardItem bordered style={{backgroundColor:"#f2f2f2",borderRadius:responsiveWidth(5)}}>
                        <TouchableOpacity  onPress={()=>{Alert.alert(" open t&c page ")}} style={{flexDirection:"row"}}>
                            <Left>
                                <Thumbnail source={require("../../../../assets/icon/tc.png")} 
                                        style={{height:responsiveWidth(10),width:responsiveWidth(10)}}/>
                                <Text style={{fontWeight:"400",marginLeft:responsiveWidth(3),fontSize:responsiveFontSize(2)}}>
                                    Terms and Conditions
                                </Text>
                            </Left>
                            <Right>
                                    <Icon name="ios-arrow-forward" type="Ionicons"/>
                            </Right>
                        </TouchableOpacity>
                    </CardItem>
                    <CardItem bordered style={{backgroundColor:"#f2f2f2",borderRadius:responsiveWidth(5)}}>
                        <TouchableOpacity  onPress={()=>{Alert.alert(" open privacy policy page ")}} style={{flexDirection:"row"}}>
                            <Left>
                                <Thumbnail source={require("../../../../assets/icon/privacypolicy.jpg")} 
                                        style={{height:responsiveWidth(10),width:responsiveWidth(10)}}/>
                                <Text style={{fontWeight:"400",marginLeft:responsiveWidth(3),fontSize:responsiveFontSize(2)}}>
                                    Privacy Policy
                                </Text>
                            </Left>
                            <Right>
                                    <Icon name="ios-arrow-forward" type="Ionicons"/>
                            </Right>
                        </TouchableOpacity>
                    </CardItem>
                    <CardItem bordered style={{backgroundColor:"#f2f2f2",borderRadius:responsiveWidth(5)}}>
                        <TouchableOpacity  onPress={()=>{Alert.alert(" open license page ")}} style={{flexDirection:"row"}}>
                            <Left>
                                <Thumbnail source={require("../../../../assets/icon/license.png")} 
                                        style={{height:responsiveWidth(10),width:responsiveWidth(10)}}/>
                                <Text style={{fontWeight:"400",marginLeft:responsiveWidth(3),fontSize:responsiveFontSize(2)}}>
                                    Licenses
                                </Text>
                            </Left>
                            <Right>
                                    <Icon name="ios-arrow-forward" type="Ionicons"/>
                            </Right>
                        </TouchableOpacity>
                    </CardItem>
                </Card>
               
            </Content>
        </LinearGradient> 
        </SafeAreaView>
)}
}
