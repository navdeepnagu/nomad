import React, { Component } from 'react';
import {View, Text, StyleSheet, Button, TouchableOpacity,Alert} from "react-native";
import { Container, Content, List, ListItem, Left, Body, Right, Thumbnail ,Icon,Separator, Card,CardItem} from 'native-base';
import HeaderCOM from "../../../../../components/Header";
import { responsiveWidth,responsiveHeight,responsiveFontSize } from 'react-native-responsive-dimensions';
import { SafeAreaView } from 'react-native-safe-area-context';

export default class HistoryList extends Component {
  render() {
    return (
  <SafeAreaView style={{flex:1}}>
    <View style={{flex:0.1, backgroundColor:"#f11", flexDirection:"row", alignItems:"center"}}>
      
       <TouchableOpacity onPress={()=>{}} style={{marginLeft:10, width:30}}>
          <Icon name="menu" type="Feather" style={{color:"white", marginTop:4}}/>
          </TouchableOpacity>
          <Text style={{color:"white", fontSize:responsiveFontSize(3), marginLeft:10}}>My Rides</Text>
     
     
    </View>
    {/* <HeaderCOM {...this.props} name="Your Rides" btnValue={true} btnTitle="menu" 
                 onAction = {()=>{}}/> */}
    <Content padder>
      <Card>
      <TouchableOpacity onPress={()=>this.props.navigation.navigate('historylistcontent')}>
        <CardItem header bordered style={{backgroundColor:"#b80f0a",height:responsiveHeight(8)}}>
          <View style={{flexDirection:"row"}}>
              <Icon  name="car-side" type="FontAwesome5" style={{color:"white"}} />
              <View style={{marginLeft:responsiveWidth(3)}}>
                  <Text style={{fontSize:responsiveFontSize(2),fontWeight:"bold", color:"white"}}>Wed, Jan 29, 06:40 PM</Text>
                  <Text style={{fontSize:responsiveFontSize(2), color:"white"}}>Car - CRN 46597336538</Text>
                  
              </View>
          </View>
          <Right>
            <Text style={{fontWeight:"bold",fontSize:responsiveFontSize(2.5), color:"white"}}> $30</Text>
          </Right>
        </CardItem>
        <CardItem style={{height:responsiveHeight(20),backgroundColor:"#f2f2f2"}}>
          <View style={{flexDirection:"row"}}>
              <Icon />
              <View style={{marginLeft:responsiveWidth(3)}}>
                <Text style={{margin:responsiveWidth(2.5)}}>Start Address</Text>
                <Text style={{margin:responsiveWidth(2.5)}}>End Address</Text>   
              </View>
          </View>
          <Body/>
          <Right>
            <Thumbnail source={require('../../../../../assets/backimg.jpg')} />
          </Right>
        </CardItem>
        </TouchableOpacity>
       </Card>
       <Text/>
       <Card>
      <TouchableOpacity onPress={()=>this.props.navigation.navigate('historylistcontent')}>
        <CardItem header bordered style={{backgroundColor:"#b80f0a",height:responsiveHeight(8)}}>
          <View style={{flexDirection:"row"}}>
              <Icon  name="car-side" type="FontAwesome5" style={{color:"white"}}/>
              <View style={{marginLeft:responsiveWidth(3)}}>
                  <Text style={{fontSize:responsiveFontSize(2),fontWeight:"bold",color:"white"}}>Wed, Jan 29, 06:40 PM</Text>
                  <Text style={{fontSize:responsiveFontSize(2), color:"white"}}>Car - CRN 46597336538</Text>
                  
              </View>
          </View>
          <Right>
            <Text style={{fontWeight:"bold",fontSize:responsiveFontSize(2.5), color:"white"}}> $30</Text>
          </Right>
        </CardItem>
        <CardItem style={{height:responsiveHeight(20),backgroundColor:"#f2f2f2"}}>
          <View style={{flexDirection:"row"}}>
              <Icon />
              <View style={{marginLeft:responsiveWidth(3)}}>
                <Text style={{margin:responsiveWidth(2.5)}}>Start Address</Text>
                <Text style={{margin:responsiveWidth(2.5)}}>End Address</Text>   
              </View>
          </View>
          <Body/>
          <Right>
            <Thumbnail source={require('../../../../../assets/backimg.jpg')} />
          </Right>
        </CardItem>
        </TouchableOpacity>
       </Card>
       <Text/>
       <Card>
      <TouchableOpacity onPress={()=>this.props.navigation.navigate('historylistcontent')}>
        <CardItem header bordered style={{backgroundColor:"#b80f0a",height:responsiveHeight(8)}}>
          <View style={{flexDirection:"row"}}>
              <Icon  name="car-side" type="FontAwesome5" style={{color:"white"}} />
              <View style={{marginLeft:responsiveWidth(3)}}>
                  <Text style={{fontSize:responsiveFontSize(2),fontWeight:"bold", color:"white"}}>Wed, Jan 29, 06:40 PM</Text>
                  <Text style={{fontSize:responsiveFontSize(2), color:"white"}}>Car - CRN 46597336538</Text>
                  
              </View>
          </View>
          <Right>
            <Text style={{fontWeight:"bold",fontSize:responsiveFontSize(2.5), color:"white"}}> $30</Text>
          </Right>
        </CardItem>
        <CardItem style={{height:responsiveHeight(20),backgroundColor:"#f2f2f2"}}>
          <View style={{flexDirection:"row"}}>
              <Icon />
              <View style={{marginLeft:responsiveWidth(3)}}>
                <Text style={{margin:responsiveWidth(2.5)}}>Start Address</Text>
                <Text style={{margin:responsiveWidth(2.5)}}>End Address</Text>   
              </View>
          </View>
          <Body/>
          <Right>
            <Thumbnail source={require('../../../../../assets/backimg.jpg')} />
          </Right>
        </CardItem>
        </TouchableOpacity>
       </Card>
     </Content>
     </SafeAreaView>
    );
  }
}