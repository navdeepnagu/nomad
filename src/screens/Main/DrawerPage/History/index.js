import {createAppContainer} from "react-navigation";
import {createStackNavigator} from 'react-navigation-stack';
import HistoryList from '../History/HistoryList';
import HistoryListContent from "../History/HistoryListContent";

const Historypage = createStackNavigator({
    historylist:HistoryList,
    historylistcontent:HistoryListContent

    
 },{
     initialRouteName:'historylist',
     headerMode:null
 });
 
 export default createAppContainer(Historypage);