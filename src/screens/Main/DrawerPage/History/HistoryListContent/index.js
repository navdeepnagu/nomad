import React, { Component, useImperativeHandle } from 'react';
import { Container, Header, Content, List, ListItem, Left, Body, Right, Thumbnail, Text,View, Icon,Separator  } from 'native-base';
import { ImageBackground ,Image} from 'react-native';
import HeaderCOM from "../../../../../components/Header";
import { responsiveHeight, responsiveWidth ,responsiveFontSize} from 'react-native-responsive-dimensions';

export default class HistoryListContent extends Component {
  render() {
    return (
      <Container>
         {/* <HeaderCOM {...this.props} name="CRN 46597336538" btnValue={true} ntfBtnValue={false} btnTitle="arrow-back" 
                    onAction = {()=>{this.props.navigation.goBack()}}/> */}
        <Content>
            <View>
              <ImageBackground source={require('../../../../../assets/googlemap.png')} 
                                style={{width:responsiveWidth(100),height:responsiveHeight(25)}}/>
            </View>
            
            <ListItem thumbnail style={{margin:responsiveHeight(2)}} >
              <Left>
                <Thumbnail source={require('../../../../../assets/backimg.jpg')} />
              </Left>
              <Body>
                <Text>Kamesh Kumar</Text>
                <Text note>You rated</Text>
             </Body>
            
            </ListItem>
            <ListItem thumbnail>
              <Left>
              <Image style={{height:responsiveHeight(5),width:responsiveWidth(15)}} 
                      source={require('../../../../../assets/car.jpg')} />
                </Left>
              <Body>
                <Text>Car - Black Swift</Text> 
             </Body>
            </ListItem>
            <ListItem avatar noBorder>
              <Left>
                <Thumbnail square source={require('../../../../../assets/cash.png')} />
              </Left>
              <Body>
                <Text>₹ 25</Text>
             </Body>
            </ListItem>
            <Separator/>
            <ListItem last >
              <Body>
              
                <View style={{flexDirection:"row"}}>
                  <View>
                  <Text style={{fontSize:responsiveFontSize(2),fontWeight:"bold"}}>Wed, Jan 29</Text>
                    <Text style={{margin:7}}>06:20 AM</Text>
                    <Text style={{margin:7}}>10:10 PM</Text>
                  </View>
                  <View >
                    <Text/>
                    <Text style={{margin:7}}>start address</Text>
                    <Text style={{margin:7}}>End address</Text>
                  </View>
                </View>
             </Body>
            </ListItem>
            <Separator/>
            <ListItem itemHeader>
              <Text style={{fontWeight:"bold",color:"#000",fontSize:18}}>Bill Details</Text>
            </ListItem>
            <ListItem>
            <Left>
              <Text>Your Trip</Text>
            </Left>
            <Body/>
            <Right>
              <Text>₹ 19</Text>
            </Right>
            </ListItem>
            <ListItem>
              <Left>
                <Text>Booking Fee</Text>
              </Left>
              <Body/>
              <Right>
                <Text>₹ 05</Text>
              </Right>
            </ListItem>
            <ListItem>
              <Left>
                <Text>Rounded Off</Text>
              </Left>
              <Body/>
              <Right>
                <Text>₹ 01</Text>
              </Right>
            </ListItem>           
            <ListItem last noBorder>
              <Left>
                <View >
                  <Text style={{fontWeight:"bold"}}>Total Bill</Text>
                  <Text note>Includes All Taxes.</Text>
                </View>
              </Left>
              <Body/>
              <Right>
                <Text>₹ 25</Text>
              </Right>
            </ListItem>
            <Separator/>
            <ListItem itemHeader>
              <Left>
                <View>
                  <Text style={{fontWeight:"bold"}}>Payment</Text>
                  <Text/>
                  <Text>Cash</Text>
                </View>
              </Left>
              <Body/>
              <Right>
                <Text/><Text/>
                <Text>₹ 25</Text>
              </Right>
            </ListItem>
            
            
         
        </Content>
      </Container>
    );
  }
}