
import React from "react";
import {View,Text,TouchableOpacity,Image, ImageBackground, Alert, StatusBar} from "react-native";
import {Header,Icon,Button, Container, Content,Card,CardItem, Input} from "native-base";
import {responsiveHeight,responsiveWidth,responsiveFontSize} from "react-native-responsive-dimensions";
import { LinearGradient } from "expo-linear-gradient";
import HeaderCOM from '../../../../components/Header';
import { SafeAreaView } from "react-native-safe-area-context";


class Refer extends React.Component{
    render(){
        return(
            <SafeAreaView style={{flex:1}}>
               <StatusBar barStyle = "light-content" hidden = {false} backgroundColor = "#000" translucent = {true}/> 
               <View style={{flex:0.1, backgroundColor:"#f11",alignItems:"center", flexDirection:"row"}}>
                    <TouchableOpacity style={{marginLeft:10}}>
                        <Icon name="menu" type="Feather" style={{color:"white", marginTop:5}}/>
                    </TouchableOpacity>
                    <Text style={{color:"white", marginLeft:10, fontSize:responsiveFontSize(3)}}>Refer & Earn</Text>
               </View>
                <LinearGradient start={{x: 0, y: 1}} end={{x: 0, y: 0}} colors={['#FF0000', '#6B0101']} style={{flex:1}}>
            {/* <HeaderCOM {...this.props} 
                        name="Refer & Earn" 
                        ltbtnValue={true} 
                        rtfBtnValue={true} 
                        ltbtnTitle="arrow-back" 
                        rtbtnTitle="dots-three-vertical" 
                        onActionLtBtn = {()=>{this.props.navigation.goBack()}}
                        /> */}
            
            <Content>
            
                <View style={{justifyContent:"center",alignItems:"center",margin:responsiveWidth(5)}}>
                    <Image source={require("../../../../assets/refer.png")} style={{width:responsiveWidth(70),
                                    height:responsiveHeight(30)}}/>
                </View>
                <View style={{justifyContent:"center",alignItems:"center",margin:responsiveWidth(5)}}>
                    <Text style={{color:"#fff",fontSize:responsiveFontSize(3),fontWeight:"bold"}}>
                        Refer Your Friend, Earn Points
                    </Text>
                </View>
                <View style={{height:responsiveHeight(7),width:responsiveWidth(60),
                                marginLeft:responsiveWidth(20),flexDirection:"row",borderWidth:2,
                                borderColor:"#fff",borderRadius:responsiveWidth(5)}}>
                    <View style={{flex:2,alignItems:"center",justifyContent:"center",borderTopLeftRadius:responsiveWidth(4),
                                borderBottomLeftRadius:responsiveWidth(4)}}>
                        <Text style={{color:"#fff",fontSize:responsiveFontSize(3),fontWeight:"bold"}}>Brar1313</Text>
                    </View>
                   <View style={{flex:1,alignItems:"center",justifyContent:"center",backgroundColor:"#fff",
                                borderTopRightRadius:responsiveWidth(4),borderBottomRightRadius:responsiveWidth(4)}}>
                       <TouchableOpacity onPress={()=> Alert.alert("Copied")} >
                           <Text style={{color:"#1CB5E0",fontSize:responsiveFontSize(3),fontWeight:"bold"}}>
                               Copy
                           </Text>
                       </TouchableOpacity>
                   </View>
                </View>
                <View style={{height:responsiveHeight(7),margin:responsiveWidth(5),marginTop:responsiveHeight(7),alignItems:"center",
                                justifyContent:"center",padding:20}}>
                    <Text style={{color:"#fff",fontWeight:"bold",fontSize:responsiveFontSize(2)}}>
                        Invite your friends to join Nomad 
                    </Text>
                    <Text style={{color:"#fff",fontWeight:"bold",fontSize:responsiveFontSize(2)}}>
                        and get 100 points for each friend that
                    </Text>
                    <Text style={{color:"#fff",fontWeight:"bold",fontSize:responsiveFontSize(2)}}>
                    joins using your referal code
                    </Text>
                </View>
                <View style={{height:responsiveHeight(7),margin:responsiveWidth(5),alignItems:"center",
                                justifyContent:"center",padding:responsiveFontSize(2)}}>
                    <Text style={{color:"#fff",fontWeight:"bold",fontSize:responsiveFontSize(2)}}>
                        Your friends also gets 100 points on
                    </Text>
                    <Text style={{color:"#fff",fontWeight:"bold",fontSize:responsiveFontSize(2)}}>
                        their Nomad membership
                    </Text>
                    
                </View>
                <View style={{alignItems:"center",justifyContent:"center"}}>
                    <Button onPress={()=> Alert.alert("refer now")}
                            style={{width:responsiveWidth(30),borderRadius:responsiveWidth(5),backgroundColor:"#6b0101",
                                    margin:responsiveHeight(1),alignItems:"center",justifyContent:"center"}}>
                        <Text style={{color:"white"}}>REFER NOW</Text>
                    </Button>
                </View>
                
            </Content>
            </LinearGradient>
            </SafeAreaView>
                
        )
    }

}
export default Refer;