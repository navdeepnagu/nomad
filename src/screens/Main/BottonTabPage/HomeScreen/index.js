
import React from 'react';
import {View, Text,StyleSheet,TouchableOpacity,ImageBackground,Alert,StatusBar} from "react-native";
import { Container,Content,Icon} from 'native-base';
import { responsiveWidth, responsiveHeight, responsiveFontSize } from 'react-native-responsive-dimensions';
import { LinearGradient } from 'expo-linear-gradient';
import { SafeAreaView  } from 'react-navigation';
import {DrawerActions} from "react-navigation-drawer"


export default class HOMESCREEN extends React.Component {
    constructor(props){
        super(props);
    }
render(){
 return (
     <SafeAreaView style={{flex:1, marginTop:20}}>
         <StatusBar barStyle = "light-content" hidden = {false} backgroundColor = "#6b0101" translucent = {true}/>
   
        <ImageBackground source={require('../../../../assets/home.png')} resizeMode="stretch" style={{flex:1}}>
            
            
           
                <View style={{flexDirection:"row", marginTop:20}}>
                    <View style={{flex:1,marginLeft:responsiveWidth(3)}}>
                        <TouchableOpacity onPress={()=>{this.props.navigation.toggleDrawer()}}>
                            <Icon name="menu"  type="Feather" style={{color:"#fff", marginLeft:10}}/>
                        </TouchableOpacity>
                    </View>
                    <View style={{flex:1,alignItems:"flex-end",marginRight:responsiveWidth(3)}}>
                        <TouchableOpacity onPress={()=>Alert.alert("notification")}>
                            <Icon name="bell" type="FontAwesome" style={{color:"#fff", marginRight:10}}/>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{marginTop:responsiveHeight(8)}}>
                    <Text style={{color:'#fff',fontSize:responsiveFontSize(5),margin:responsiveWidth(5),
                                fontWeight:"bold", textShadowRadius:10, textShadowColor:"black"}}>NOMAD</Text>
                    <Text  style={{color:'#fff',fontSize:responsiveFontSize(3),marginLeft:responsiveWidth(5),
                                fontWeight:"bold"}}>
                        Find Your Pool
                    </Text>
                    <Text style={{color:'#fff',fontSize:responsiveFontSize(3),margin:responsiveWidth(5)}}>
                        It's the journey,  not the destination that matters
                    </Text>
                </View>
                <View style={{marginTop:responsiveHeight(15),alignItems:"center",justifyContent:"center"}}>
                    <TouchableOpacity onPress={()=> this.props.navigation.navigate('findride')}
                                style={{margin:responsiveWidth(5),width:responsiveWidth(50)}}>
                        <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={['#FF0000', '#6B0101']} 
                                    style={{borderRadius: responsiveWidth(5),height:responsiveHeight(8),width:responsiveWidth(50),
                                            alignItems:"center",justifyContent:"center"}}>
                            <Text style={{color:'white',fontSize:responsiveFontSize(2)}}>Find Ride</Text>
                        </LinearGradient>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=> this.props.navigation.navigate('offerride')}
                            style={{margin:responsiveWidth(5),width:responsiveWidth(50)}}>
                        <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={['#FF0000', '#6B0101']}
                                    style={{borderRadius: responsiveWidth(5),height:responsiveHeight(8),width:responsiveWidth(50),
                                            alignItems:"center",justifyContent:"center"}}>
                            <Text style={{color:'white',}}>Offer Ride</Text>
                        </LinearGradient>
                    </TouchableOpacity>
                </View>
           
        </ImageBackground>
    
    </SafeAreaView>
);}
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        alignItems:"center",
        justifyContent:"center"
    }
});