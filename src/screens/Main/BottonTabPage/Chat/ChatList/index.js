import React from 'react';
import {View, Text, StyleSheet, Button, TouchableOpacity,Alert, ImageBackground} from "react-native";
import { Container, Content, List, ListItem, Left, Body, Right, Thumbnail,Icon} from 'native-base';
import HeaderCOM from '../../../../../components/Header';
import {SafeAreaView} from "react-native-safe-area-context"
import {responsiveFontSize} from "react-native-responsive-dimensions"


export default class ChatList extends React.Component {
    constructor(props){
        super(props);
    }
    render(){
       // console.log(this.props)
        return (
            <SafeAreaView style={{flex:1}}>
              <View style={{flex:0.1, backgroundColor:"red", flexDirection:"row", alignItems:"center", justifyContent:"space-between"}}>
                <View style={{flexDirection:"row"}}>
                  <TouchableOpacity onPress={()=>this.props.navigation.goBack()}>
                    <Icon name="menu" type="Feather" style={{color:"white", marginLeft:10, marginTop:5}}/>
                  </TouchableOpacity>
                  <Text style={{color:"white", fontSize:responsiveFontSize(3), marginLeft:10}}>Inbox</Text>
                </View>
                <View>
                  <TouchableOpacity onPress={()=>{}}>
                    <Icon name="dots-vertical" type="MaterialCommunityIcons" style={{color:"white"}}/>
                  </TouchableOpacity>
                </View>
              </View>
       {/* <HeaderCOM {...this.props} name="Inbox" btnValue={true} ntfBtnValue={false} btnTitle="menu" 
                    onAction = {()=>{}}/> */}
              <ImageBackground source={require("../../../../../assets/bgnew.jpg")} style={{width:"100%", height:"100%", flex:1}}>
        <Content>
        <List>
            <ListItem avatar>
              <Left>
                <Thumbnail source={require('../../../../../assets/backimg.jpg')} />
              </Left>
              <Body>
              <TouchableOpacity onPress={()=>this.props.navigation.navigate('chatlistcontent')}>
                <Text style={{fontWeight:"bold", color:"red"}}>Kumar Pratik</Text>
                <Text note style={{color:"red"}}>Doing what you like will always keep you happy . .</Text>
                </TouchableOpacity>
              </Body>
              <Right style={{justifyContent:"center"}}>
              <TouchableOpacity onPress={()=>this.props.navigation.navigate('chatlistcontent')}>
              <Icon name="right" type="AntDesign" />
              </TouchableOpacity>
              </Right>
            </ListItem>
            <ListItem avatar>
              <Left>
                <Thumbnail source={require('../../../../../assets/backimg.jpg')} />
              </Left>
              <Body>
              <TouchableOpacity onPress={()=>this.props.navigation.navigate('chatlistcontent')}>
                <Text style={{fontWeight:"bold", color:"red"}}>Kumar Pratik</Text>
                <Text note style={{color:"red"}}>Doing what you like will always keep you happy . .</Text>
                </TouchableOpacity>
              </Body>
              <Right  style={{justifyContent:"center"}}>
              <TouchableOpacity onPress={()=>this.props.navigation.navigate("chatlistcontent")}>
              <Icon name="right" type="AntDesign" />
              </TouchableOpacity>
              </Right>
            </ListItem>
           
          </List>
        </Content>
        </ImageBackground>
        </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        alignItems:"center",
        justifyContent:"center"
    }
});