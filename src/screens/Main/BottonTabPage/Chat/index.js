import {createAppContainer} from "react-navigation";
import {createStackNavigator} from 'react-navigation-stack';
import ChatList from '../Chat/ChatList';
import ChatListContent from '../Chat/ChatListContent';

const Chatpage = createStackNavigator({
    chatlist:ChatList,
    chatlistcontent:ChatListContent

    
 },{
     initialRouteName:'chatlist',
     headerMode:null
 });
 
 export default createAppContainer(Chatpage);