import React, { Component } from 'react';
import { View, Image ,Text, StyleSheet, ImageBackground, Alert,AsyncStorage, TouchableOpacity} from 'react-native';
import { Container, Content, List, ListItem, Left, Right, Icon,Separator, Body, Button, Tab,Tabs } from 'native-base';
import HeaderCOM from '../../../../components/Header';
import MyDetails from './myDetails';
import ProfileSetting from './setting';
import {SocialIcon} from 'react-native-elements';
import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';
import {SafeAreaView} from "react-native-safe-area-context"

export default class App extends Component {
  constructor(props){
    super(props);
    this.state={
      name:"",
      mail:"",
      photo:""
    }

}
  getdata=async()=>{
    var  user_name=await AsyncStorage.getItem('Name');
    const name = JSON.parse(user_name);
     var user_email=await AsyncStorage.getItem('Email');
     const uMail = JSON.parse(user_email);
     var user_photo=await AsyncStorage.getItem('Photo');
     const uPhoto = JSON.parse(user_photo);
    this.setState({name:name,mail:uMail,photo:uPhoto});
   }
  render() {
    this.getdata();
    return (
        <SafeAreaView style={{flex:1}}>
            {/* <HeaderCOM {...this.props} name="Profile" btnValue={true} ntfBtnValue={false} btnTitle="menu" 
                    onAction = {()=>{}}/> */}
              <View style={{flex:0.1, backgroundColor:"red", flexDirection:"row", alignItems:"center", justifyContent:"space-between"}}>
                <View style={{flexDirection:"row"}}>
                  <TouchableOpacity onPress={()=>this.props.navigation.goBack()}>
                    <Icon name="menu" type="Feather" style={{color:"white", marginLeft:10, marginTop:5}}/>
                  </TouchableOpacity>
                  <Text style={{color:"white", fontSize:responsiveFontSize(3), marginLeft:10}}>Profile</Text>
                </View>
                <View>
                  <TouchableOpacity onPress={()=>{}}>
                    <Icon name="dots-vertical" type="MaterialCommunityIcons" style={{color:"white"}}/>
                  </TouchableOpacity>
                </View>
              </View>
               
              
              <ImageBackground source={require("../../../../assets/bgnew.jpg")} style={{width:"100%", height:"100%", flex:1}}>
                <View style={{flexDirection:"row",marginLeft:responsiveWidth(5)}}>
                  <View style={{backgroundColor:"red",marginLeft:100, height:responsiveWidth(30),width:responsiveWidth(30),marginTop:responsiveHeight(2),
                                    borderRadius:responsiveWidth(15),borderWidth:responsiveWidth(1),borderColor:"#fff"}}>
                      <Image //source={{uri:this.state.photo}}
                            style={{height:responsiveWidth(28),width:responsiveWidth(28),borderRadius:responsiveWidth(14)}}/>
                      <View>
                        <Button onPress={()=> Alert.alert("open image picker")}
                                style={{marginTop:responsiveHeight(-5),marginLeft:responsiveWidth(20),
                                      height:responsiveHeight(6),width:responsiveHeight(7),borderRadius:responsiveHeight(3),
                                      backgroundColor:"#000046",borderColor:"#fff",borderWidth:responsiveWidth(1),justifyContent:"center"}}>
                          <Text style={{color:"#fff"}}>Edit</Text>
                        </Button>
                      </View>
                      <View style={{width:270,height:50}}>
                                <Text style={{color:"black"}}>{this.state.mail}</Text>
                                </View>
                  </View>
                  </View>
                 
              </ImageBackground>
            </SafeAreaView>
       
    );
  }
}

const styles = StyleSheet.create({
  container:{
      borderWidth:1,
      margin:10,
  },
  container1: {
    fontSize:18,
    fontWeight:"bold"
    
  }
});