import React from 'react';
import { Container,Content,Card,CardItem,Body,Icon,Item,Input} from 'native-base';
import { View ,Text,StyleSheet,TouchableOpacity, StatusBar, ImageBackground} from 'react-native';
import {responsiveHeight,responsiveWidth, responsiveFontSize} from'react-native-responsive-dimensions'
import {SafeAreaView} from "react-native-safe-area-context"
import HeaderCOM from '../../../../../components/Header';
import { LinearGradient } from 'expo-linear-gradient';
import DatePicker from 'react-native-datepicker';
import Modal from "../../../../../components/Mapmodal";


export default class FindRideMain extends React.Component {
constructor(props){
    super(props);
    this.state = { chosenDate: new Date(),
                    time : new Date(),
                    showModal:false,
                    showModal1:false,
                    pickupval:"",
                    destval:"",

                          };
}
setval(pickup,show){
    this.setState({pickupval:pickup,showModal:!show})
} 
setvaldest(dest,show){
  this.setState({destval:dest,showModal1:!show})
}
render(){
  return (
    <SafeAreaView style={{flex:1}}>
      <StatusBar barStyle = "light-content" hidden = {false} backgroundColor = "#000" translucent = {true}/>
      <View style={{flex:0.1, backgroundColor:"red", flexDirection:"row", alignItems:"center", justifyContent:"space-between"}}>
        <View style={{flexDirection:"row"}}>
        <TouchableOpacity onPress={()=>this.props.navigation.goBack()}>
          <Icon name="menu" type="Feather" style={{color:"white", marginLeft:10, marginTop:5}}/>
        </TouchableOpacity>
        <Text style={{color:"white", fontSize:responsiveFontSize(3), marginLeft:10}}>Find a pool</Text>
        </View>
        <View>
        <TouchableOpacity onPress={()=>{}}>
          <Icon name="dots-vertical" type="MaterialCommunityIcons" style={{color:"white"}}/>
        </TouchableOpacity>
        </View>
      </View>
       
      <ImageBackground source={require("../../../../../assets/bgnew.jpg")} style={{width:"100%", height:"100%", flex:1}}>
            {/* <HeaderCOM {...this.props} name="Find a pool" ltbtnValue={true} rtfBtnValue={true} ltbtnTitle="menu" 
                rtbtnTitle="dots-three-vertical" onAction = {()=>{}}/> */}
              <Content padder>
                <Card style={{backgroundColor:"#f2f2f2",borderRadius:responsiveWidth(5)}}>
                  <CardItem header nobordered style={{backgroundColor:"#d9d9d9",borderRadius:responsiveWidth(5)}}>
                    <Text style={{fontWeight:"bold",fontSize:responsiveFontSize(2.5)}}>Select a Location</Text>
                  </CardItem>
                  <CardItem bordered style={{backgroundColor:"#f2f2f2",borderRadius:responsiveWidth(5)}}>
                    <Body style={{alignItems:"center"}}>
                      <Item rounded >
                        <Icon active name='map-marker-alt'  type="FontAwesome5" style={{color:"green"}}/>
                        <Input placeholder="Leaving From" 
                              value={this.state.pickupval}
                              style={{width:responsiveWidth(80)}}
                              onTouchStart={()=>this.setState({showModal:true})}/>
                      </Item>
                      <Text/>
                      <Item rounded >
                        <Icon active name='map-marker-alt'  type="FontAwesome5" style={{color:"red"}}/>
                        <Input placeholder="Going To" style={{width:responsiveWidth(80),}}
                              value={this.state.destval}
                              onTouchStart={()=>this.setState({showModal1:true})}/>
                      </Item>
                      <Text/>
                    </Body>
                  </CardItem>
                </Card>
                <Text/>
                <Card style={{backgroundColor:"#f2f2f2",borderRadius:responsiveWidth(5)}}>
                  <CardItem header nobordered style={{backgroundColor:"#d9d9d9",borderRadius:responsiveWidth(5)}}>
                    <Text style={{fontWeight:"bold",fontSize:responsiveFontSize(2.5)}}>Select Date & Time</Text>
                  </CardItem>
                  <CardItem nobordered style={{backgroundColor:"#f2f2f2",borderRadius:responsiveWidth(5),
                                            marginTop:responsiveHeight(2),marginBottom:responsiveHeight(2)}}> 
                    <DatePicker
                      date={this.state.chosenDate} 
                      mode="date" 
                      format="DD-MM-YYYY"
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      customStyles={{
                          dateIcon: {position: 'absolute',
                                    left: 0,
                                    top: 4,
                                    marginLeft: 0
                                  },
                          dateInput: {marginLeft: 36
                                  }
                        }}
                      onDateChange={(date) => {this.setState({chosenDate: date})}}
                    />
                    <Icon active name='clock'  type="FontAwesome5" style={{marginLeft:responsiveWidth(5),color:"#ff5c33"}}/> 
                    <DatePicker 
                      date={this.state.time} 
                      mode="time" 
                      format="hh:mm A"
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      iconComponent={<Icon/>}
                      onDateChange={(time) => {this.setState({time: time})}}
                    />
                  </CardItem>
                </Card>
                <View style={{alignItems:"center",justifyContent:"center"}}>
                  <TouchableOpacity onPress={()=> this.props.navigation.navigate("findRideList")}
                                  style={{margin:responsiveWidth(5),width:responsiveWidth(50)}}>
                    <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={['#FF0000', '#6B0101']}
                                  style={{borderRadius: responsiveWidth(5),height:responsiveHeight(8),width:responsiveWidth(50),
                                        alignItems:"center",justifyContent:"center"}}>
                      <Text style={{color:'white'}}>Find Ride</Text>
                    </LinearGradient>
                  </TouchableOpacity>
                </View>
              </Content>
            {this.state.showModal && 
              <Modal onRef = {ref => this.Modal = ref} 
                    onAction = {(pick,show) => this.setval(pick,show)}/>
            }
            {this.state.showModal1 && 
              <Modal onRef = {ref => this.Modal = ref}
                    okbtn = {true}
                    cnlbtn = {true}
                    onAction = {(dest,show) => this.setvaldest(dest,show)}/>
            }
      
        </ImageBackground>
        </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        alignItems:"center",
        justifyContent:"center"
    }
});