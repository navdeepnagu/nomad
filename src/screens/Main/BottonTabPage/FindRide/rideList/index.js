import React from "react";
import {View,Text,TouchableOpacity} from "react-native";
import {Container,Content,Card,CardItem,Body,Icon,Right,Thumbnail} from "native-base";
import {responsiveHeight,responsiveWidth,responsiveFontSize} from "react-native-responsive-dimensions";
import { LinearGradient } from "expo-linear-gradient";
import HeaderCOM from '../../../../../components/Header';

export default class FindRideList extends React.Component{
constructor(props){
    super(props);
    this.state = {};
}
render(){
return(
    <Container>
        {/* <HeaderCOM {...this.props} name="Search List" ltbtnValue={true} rtfBtnValue={true} ltbtnTitle="arrow-back" 
                   rtbtnTitle="dots-three-vertical" onAction = {()=>{this.props.navigation.goBack()}}/> */}
            <LinearGradient start={{x: 0, y: 0}} end={{x: 0, y: 1}} colors={['#1CB5E0', '#000046']} style={{flex:1}}>
                <Content padder>
                    <View style={{flexDirection:"row",marginLeft:responsiveWidth(5)}}>
                        <View >
                            <Icon active name='map-marker-alt'  type="FontAwesome5" 
                                style={{color:"green",marginLeft:responsiveWidth(1),fontSize:responsiveFontSize(3)}}/>
                            <Icon active name='dots-three-vertical'  type="Entypo" 
                                style={{color:"#fff",fontSize:responsiveFontSize(3)}}/>  
                        </View>
                        <View style={{flex:1}}>
                            <Text style={{color:"#fff",fontWeight:"bold",fontSize:responsiveFontSize(2.5),
                                        marginLeft:responsiveWidth(5)}}>
                                Chandigarh
                            </Text>
                        </View>
                    </View>
                    <View style={{flexDirection:"row",marginLeft:responsiveWidth(5)}}>
                        <View >
                            <Icon active name='map-marker-alt'  type="FontAwesome5" 
                                style={{color:"red",marginLeft:responsiveWidth(1),fontSize:responsiveFontSize(3)}}/>
                        </View>
                        <View style={{flex:1,justifyContent:"flex-end"}}>
                            <Text style={{color:"#fff",fontWeight:"bold",fontSize:responsiveFontSize(2.5),
                                        marginLeft:responsiveWidth(5)}}>
                                Sonipat
                            </Text>
                        </View>
                    </View>
                    <View style={{flexDirection:"row",marginTop:responsiveWidth(5)}}>
                        <View style={{flex:1,}}>
                            <Text style={{color:"#fff",fontWeight:"bold",fontSize:responsiveFontSize(2),}}>
                                    March,20
                            </Text>
                        </View>
                        <View style={{flex:1,alignItems:"flex-end"}}>
                            <Text note style={{color:"#fff"}}>
                                    Aerage time:8 hr
                            </Text>
                        </View>
                    </View>
                    <Card style={{backgroundColor:"#f2f2f2",borderRadius:responsiveWidth(10),marginTop:responsiveWidth(5)}}>
                        <TouchableOpacity onPress={()=> this.props.navigation.navigate('findRideDetail')}>
                            <CardItem header nobordered style={{backgroundColor:"#f2f2f2",borderRadius:responsiveWidth(10)}}>
                                <Body>
                                    <View style={{flexDirection:"row"}}>
                                        <View >
                                            <Icon active name='map-marker-alt'  type="FontAwesome5" 
                                                    style={{color:"green",marginLeft:responsiveWidth(1),
                                                            fontSize:responsiveFontSize(3)}}/>
                                            <Icon active name='dots-three-vertical'  type="Entypo" 
                                                style={{color:"#000",fontSize:responsiveFontSize(3)}}/>
                                        </View>
                                        <View style={{flexDirection:"row"}}>
                                            <Text style={{fontWeight:"bold",fontSize:responsiveFontSize(2),
                                                        marginLeft:responsiveWidth(2)}}>
                                                10:10 AM
                                            </Text>
                                            <Text style={{color:"grey",fontWeight:"bold",fontSize:responsiveFontSize(2),
                                                        marginLeft:responsiveWidth(2)}}>
                                                Chandigarh
                                            </Text>
                                        </View>
                                        <Right>
                                            <Text style={{fontWeight:"bold",fontSize:responsiveFontSize(3)}}>
                                                500 ₹
                                            </Text>
                                        </Right>
                                    </View>
                                    <View style={{flexDirection:"row",}}>
                                        <View >
                                            <Icon active name='map-marker-alt'  type="FontAwesome5" 
                                                style={{color:"red",marginLeft:responsiveWidth(1),fontSize:responsiveFontSize(3)}}/>
                                            <Icon active name='dots-three-vertical'  type="Entypo" 
                                                style={{color:"#000",fontSize:responsiveFontSize(3)}}/>
                                            <Icon active name='dots-three-vertical'  type="Entypo" 
                                                style={{color:"#000",fontSize:responsiveFontSize(3)}}/>
                                        </View>
                                        <View style={{flexDirection:"row"}}>
                                            <Text style={{fontWeight:"bold",fontSize:responsiveFontSize(2),
                                                        marginLeft:responsiveWidth(2)}}>
                                                06:20 PM
                                            </Text>
                                            <Text style={{color:"grey",fontWeight:"bold",fontSize:responsiveFontSize(2),
                                                        marginLeft:responsiveWidth(2)}}>
                                                Sonipat
                                            </Text>
                                        </View>
                                    </View>
                                    <View style={{flexDirection:"row",marginLeft:responsiveWidth(4),
                                                marginTop:responsiveHeight(-2)}}>
                                        <View style={{alignItems:"center"}}>
                                            <Icon active name='dots-three-horizontal'  type="Entypo" 
                                                style={{color:"#000",fontSize:responsiveFontSize(3)}}/>
                                        </View>
                                        <View style={{alignItems:"center",marginLeft:responsiveWidth(1)}}>
                                            <Icon name="car" type="FontAwesome5" 
                                                style={{color:"green",fontSize:responsiveFontSize(3)}}/>
                                            <Icon active name='dots-three-vertical'  type="Entypo" 
                                                style={{color:"#000",fontSize:responsiveFontSize(3)}}/>
                                        </View>
                                        <View style={{alignItems:"center",marginLeft:responsiveWidth(2)}}>
                                            <Text style={{fontWeight:"bold"}}>
                                                Tata Nexon
                                            </Text>
                                        </View>
                                        <Right style={{marginRight:responsiveWidth(5)}}>
                                            <Thumbnail  source={require("../../../../../assets/user.png")} 
                                                        style={{height:responsiveWidth(10),width:responsiveWidth(10)}}/>
                                        </Right>  
                                    </View>
                                    <View style={{flexDirection:"row",marginLeft:responsiveWidth(10)}}>
                                        <View style={{alignItems:"center",marginLeft:responsiveWidth(1)}}>
                                            <Icon name="steering" type="MaterialCommunityIcons" 
                                                style={{color:"green",fontSize:responsiveFontSize(3)}}/>
                                        </View>
                                        <View style={{marginLeft:responsiveWidth(2)}}>
                                            <View>
                                                <Text style={{fontWeight:"bold"}}>
                                                    Rahul Singh
                                                </Text>
                                            </View>
                                            <View style={{flexDirection:"row",alignItems:"center"}}>
                                                <Text >
                                                    25 y/o
                                                </Text>
                                                <Icon name="star" style={{color:"gold",marginLeft:responsiveWidth(5)}}></Icon>
                                                <Text >
                                                    4.6
                                                </Text>
                                            </View>
                                        </View>
                                    </View>
                                </Body>      
                            </CardItem>
                        </TouchableOpacity>
                    </Card>
                </Content>
            </LinearGradient> 
    </Container>
)}   
}
