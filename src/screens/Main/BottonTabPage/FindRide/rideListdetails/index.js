import React from "react";
import {View,Text,TouchableOpacity,Alert} from "react-native";
import {Container,Content,Card,CardItem,Body,Button,Icon,Right,Left,Thumbnail} from "native-base";
import {responsiveHeight,responsiveWidth,responsiveFontSize} from "react-native-responsive-dimensions";
import { LinearGradient } from "expo-linear-gradient";
import HeaderCOM from '../../../../../components/Header';

export default class findRideDetail extends React.Component{
constructor(props){
   super(props);
   this.state = { 
            countPeople:3,
            }

}
seatAvailable = (seat) => {
    var seatClr =[];
    for  (let i = 1; i <= 6; i++)  {
        if(i>seat){
            var seatIcon = (
                <Icon key={i} name="user-alt"type="FontAwesome5" style={{color:"#000", fontSize:responsiveFontSize(3)}}/>
                );
        } 
        else {
            var seatIcon = (
                <Icon key={i} name="user-alt"type="FontAwesome5" style={{color:"green",fontSize:responsiveFontSize(3)}}/>
                );
        }
        seatClr[i] = (seatIcon); 
        };
        return seatClr;   
}  
render(){
return(
    <Container>
        {/* <HeaderCOM {...this.props} name="Tata Nexon" ltbtnValue={true} rtfBtnValue={true} ltbtnTitle="arrow-back" 
                rtbtnTitle="dots-three-vertical" onAction = {()=>{this.props.navigation.goBack()}}/> */}
            <LinearGradient start={{x: 0, y: 0}} end={{x: 0, y: 1}} colors={['#1CB5E0', '#000046']}
                            style={{flex:1}}>
                <Content padder>
                    <Card style={{backgroundColor:"#f2f2f2",borderRadius:responsiveWidth(10),marginTop:responsiveWidth(5)}}>
                        <CardItem header nobordered style={{backgroundColor:"#f2f2f2",borderRadius:responsiveWidth(10)}}>
                            <Body>
                                <View style={{flexDirection:"row"}}>
                                    <View >
                                        <Icon active name='map-marker-alt'  type="FontAwesome5" 
                                            style={{color:"green",marginLeft:responsiveWidth(1),fontSize:responsiveFontSize(3)}}/>
                                        <Icon active name='dots-three-vertical'  type="Entypo" 
                                                style={{color:"#000",fontSize:responsiveFontSize(3)}}/>
                                        <Icon active name='dots-three-vertical'  type="Entypo" 
                                                style={{color:"#000",fontSize:responsiveFontSize(3)}}/>
                                    </View>
                                    <View style={{flexDirection:"row"}}>
                                        <Text style={{fontWeight:"bold",fontSize:responsiveFontSize(2.5),
                                                    marginLeft:responsiveWidth(3)}}>
                                            10:10 AM
                                        </Text>
                                        <View style={{marginLeft:responsiveWidth(2)}}>
                                            <Text style={{color:"grey",fontWeight:"bold",fontSize:responsiveFontSize(2.5)}}>
                                                Chandigarh
                                            </Text>
                                            <Text style={{color:"grey"}} >sector-17, ISBT,Chandigarh</Text> 
                                        </View>
                                    </View>
                                </View>
                                <View style={{flexDirection:"row",}}>
                                    <View >
                                        <Icon active name='map-marker-alt'  type="FontAwesome5" 
                                            style={{color:"red",marginLeft:responsiveWidth(1),fontSize:responsiveFontSize(3)}}/>
                                        <Icon active name='dots-three-vertical'  type="Entypo" 
                                            style={{color:"#000",fontSize:responsiveFontSize(3)}}/>
                                        <Icon active name='dots-three-vertical'  type="Entypo" 
                                            style={{color:"#000",fontSize:responsiveFontSize(3)}}/>
                                        <Icon active name='dots-three-vertical'  type="Entypo" 
                                            style={{color:"#000",fontSize:responsiveFontSize(3)}}/>
                                    </View>
                                    <View style={{flexDirection:"row"}}>
                                        <Text style={{fontWeight:"bold",fontSize:responsiveFontSize(2.5),
                                                    marginLeft:responsiveWidth(3)}}>
                                            06:20 PM
                                        </Text>
                                        <View style={{marginLeft:responsiveWidth(2)}}>
                                            <Text style={{color:"grey",fontWeight:"bold",fontSize:responsiveFontSize(2.5)}}>
                                                Sonipat
                                            </Text>
                                            <Text style={{color:"grey"}} >Omaxe city, murthal,sonipat</Text> 
                                        </View>
                                    </View>
                                </View>
                                <View style={{flexDirection:"row",marginLeft:responsiveWidth(4),
                                                marginTop:responsiveHeight(-2)}}>
                                    <View style={{alignItems:"center"}}>
                                        <Icon active name='dots-three-horizontal'  type="Entypo" 
                                            style={{color:"#000",fontSize:responsiveFontSize(3)}}/>
                                    </View>
                                    <View style={{alignItems:"center",marginLeft:responsiveWidth(1)}}>
                                        <Icon name="car" type="FontAwesome5" 
                                            style={{color:"green",fontSize:responsiveFontSize(3)}}/>
                                        <Icon active name='dots-three-vertical'  type="Entypo" 
                                            style={{color:"#000",fontSize:responsiveFontSize(3)}}/>
                                    </View>
                                    <View style={{alignItems:"center",marginLeft:responsiveWidth(2)}}>
                                        <Text style={{fontWeight:"bold"}}>
                                            Tata Nexon
                                        </Text>
                                    </View>
                                    <Right style={{marginRight:responsiveWidth(5)}}>
                                        <Thumbnail  source={require("../../../../../assets/user.png")} 
                                                    style={{height:responsiveWidth(10),width:responsiveWidth(10)}}/>
                                    </Right>  
                                </View> 
                                <View style={{flexDirection:"row",marginLeft:responsiveWidth(10)}}>
                                    <View style={{alignItems:"center",marginLeft:responsiveWidth(1)}}>
                                        <Icon name="steering" type="MaterialCommunityIcons" 
                                            style={{color:"green",fontSize:responsiveFontSize(3)}}/>
                                    </View>
                                    <View style={{marginLeft:responsiveWidth(2)}}>
                                        <View>
                                            <Text style={{fontWeight:"bold"}}>
                                                Rahul Singh
                                            </Text>
                                        </View>
                                        <View style={{flexDirection:"row",alignItems:"center"}}>
                                            <Text >
                                                25 y/o
                                            </Text>
                                            <Icon name="star" style={{color:"gold",marginLeft:responsiveWidth(5)}}></Icon>
                                            <Text >
                                                4.6
                                            </Text>
                                        </View>
                                    </View>
                                </View>
                            </Body>    
                        </CardItem>
                        <CardItem nobordered style={{backgroundColor:"#f2f2f2",borderRadius:responsiveWidth(10)}}>
                            <Left>
                                <Text style={{fontWeight:"bold",fontSize:responsiveFontSize(2.5)}}>
                                    March,20
                                </Text>
                            </Left>
                            <Right>
                                <Text >
                                    Average Time:8 hr
                                </Text>
                            </Right>
                        </CardItem>
                        <CardItem nobordered style={{backgroundColor:"#f2f2f2",borderRadius:responsiveWidth(10)}}>
                            <Text style={{fontWeight:"bold",fontSize:responsiveFontSize(2.5)}}>
                                No. of seats available : {this.state.countPeople}
                            </Text>
                        </CardItem>
                        <CardItem  style={{backgroundColor:"#f2f2f2",borderRadius:responsiveWidth(10)}}>
                            {this.seatAvailable(this.state.countPeople)}
                        </CardItem>
                        <CardItem style={{backgroundColor:"#f2f2f2",borderRadius:responsiveWidth(10)}}>
                            <Icon name="paw-off" type="MaterialCommunityIcons" style={{color:"red"}}/>
                            <Text note>
                                No pets
                            </Text>
                            <Icon name="smoking-off" type="MaterialCommunityIcons" style={{color:"red",marginLeft:responsiveWidth(20)}}/>
                            <Text note>
                                No Smoking
                            </Text>
                        </CardItem>
                        <CardItem  nobordered style={{backgroundColor:"#f2f2f2",borderRadius:responsiveWidth(10),justifyContent:"center"}}>
                            <Text style={{fontWeight:"bold",fontSize:responsiveFontSize(3)}}>500 ₹</Text>
                            <Button onPress={()=>{Alert.alert("booked sucessfully")}} style={{backgroundColor:"#000046",borderRadius:responsiveWidth(5),marginLeft:responsiveWidth(20)}}>
                                <Text style={{margin:responsiveWidth(2),color:"#fff"}}>
                                    CONFIRM & BOOK
                                </Text>
                            </Button>
                        </CardItem>
                    </Card>
                </Content>
            </LinearGradient> 
        </Container>
)}
}
