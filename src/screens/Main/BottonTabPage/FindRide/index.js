import {createAppContainer,createSwitchNavigator} from "react-navigation";
import {createStackNavigator} from 'react-navigation-stack';
import FindRideMain from './rideMain';
import FindRideList from "./rideList";
import FindRideDetail from './rideListdetails';

const findRidePage = createStackNavigator({
    findRideMain:FindRideMain,
    findRideList:FindRideList,
    findRideDetail:FindRideDetail
    
 },{
     initialRouteName:'findRideMain',
     headerMode:null
 });
 
 export default createAppContainer(findRidePage);