import React, { Component } from 'react';
import { View, Text, Image,AsyncStorage ,StyleSheet,ActivityIndicator, ImageBackground} from 'react-native';
import { responsiveWidth, responsiveHeight } from 'react-native-responsive-dimensions';
import { LinearGradient } from 'expo-linear-gradient';
import Firebase from '../../config/firebase/firebase';

class Splash extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  retrieveItem = async (key) => {
    return new Promise(async (resolve, reject) => {
        try {
            const retrievedItem = await AsyncStorage.getItem(key);
            const item = JSON.parse(retrievedItem);
            resolve(item);
        } catch (error) {
            reject(error.message);
        }
    })
  }

  componentDidMount = () => {
    setTimeout(()=>{
      this.retrieveItem("Email").then((response)=>{
        if(!response){
          this.props.navigation.navigate("auth");
        } else{
          this.props.navigation.navigate("main");
        }
      }).catch((error) =>{
        alert(error)
      });
    },2000);
  } 
  
  render() {
    return (
      <View style={styles.container}>
        <ImageBackground source={require("../../assets/splash.jpg")} style={{width:"100%", height:"100%"}}>
          <ActivityIndicator color="white" size="large" style={{marginTop:400}}></ActivityIndicator>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    
  },
  loader : {
    position:"absolute",
    bottom:100,
    alignSelf : "center",
  }

})

export default Splash;