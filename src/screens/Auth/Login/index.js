import React from 'react';
import {Text, View,TouchableOpacity,AsyncStorage,SafeAreaView,TouchableWithoutFeedback,Keyboard,KeyboardAvoidingView,StatusBar,ImageBackground,Image} from 'react-native';
import {SocialIcon} from 'react-native-elements';
import {Container,Body,Icon,Input, Content,Item, Toast,Label,Card,CardItem,Spinner} from "native-base";
import {responsiveWidth,responsiveHeight,responsiveFontSize} from "react-native-responsive-dimensions";
import { LinearGradient } from 'expo-linear-gradient';
import Firebase from "../../../config/firebase/firebase";
import Loader from '../../../components/loader';

export default class Login extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      email: '',
      password: '',
      color1:"grey",
      color2:"grey",
      passwordVisibility: true,
      passwordShow:'ios-eye',
      loading:false,
      showLoader:false,

    };
  }
  handlePasswordVisibility = () => {
    this.setState({
      passwordShow: this.state.passwordShow === 'ios-eye' ? 'ios-eye-off' : 'ios-eye',
      passwordVisibility: !this.state.passwordVisibility
    })
  }
  validateEmail = ()=>{
    var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    if(emailPattern.test(this.state.email)){
        return true;
    }
    return false;
  }
  
  validatePass = ()=>{
    var passPattern = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*_-])[A-Za-z\d!@#$%^&*_-]{8,}$/;
    if(passPattern.test(this.state.password)){
        return true;
    }
    return false;
  }

  handleOnLogin = async () => {
  if(this.state.email.length > 0 && this.state.password.length > 0){
    if(!this.validateEmail())
            {
              this.setState({color1:'red',color2:"grey",loading:false})
              Toast.show({
                    text:"please fill email in correct format (abc@email.com)",
                    textStyle:{textAlign:"center"},
                    duration:2000,
                    style:{borderRadius:10,margin:responsiveWidth(10)}
                  });
            }
    else if(!this.validatePass())
          {
            this.setState({color1:"green",color2:'red',loading:false})
            Toast.show({
              text:"Password must include minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character",
              textStyle:{textAlign:"center"},
              duration:2000,
              style:{borderRadius:10,margin:responsiveWidth(10)}
            }); 
          }
     else{  this.setState({color1:"grey",color2:"grey",loading:true})
     try {
      const response = await Firebase.loginWithEmail(this.state.email, this.state.password)
      if (response.status) {
        this.storeItem("UID",response.data.user.uid);
        this.storeItem("Email",this.state.email);
        this.setState({loading:false});
        Toast.show({
          text:response.message,
          textStyle:{textAlign:"center"},
          duration:2000,
          style:{borderRadius:10,margin:responsiveWidth(10)}
        });
        this.props.navigation.navigate('main');
      }
     
      this.setState({loading:false});
    } catch (error) {
      this.setState({loading:false})
      Toast.show({
        text:error.message,
        textStyle:{textAlign:"center"},
        duration:2000,
        style:{borderRadius:10,margin:responsiveWidth(10)}
      });
      
      
    }
     
     }
     
   
  }
  else if(this.state.password.length>0)
          {
            this.setState({color1:'red'})
            Toast.show({
              text:"Please enter Email",
              textStyle:{textAlign:"center"},
              duration:2000,
              style:{borderRadius:10,margin:responsiveWidth(10)}
            }); 
          }
  else if(this.state.email.length>0)
          {
            this.setState({color2:'red'})
            Toast.show({
              text:"Please enter Password",
              textStyle:{textAlign:"center"},
              duration:2000,
              style:{borderRadius:10,margin:responsiveWidth(10)}
            }); 
          }
  else {
        Toast.show({
          text:"Fill the requried fields (*)",
          textStyle:{textAlign:"center"},
          duration:2000,
          style:{borderRadius:10,margin:responsiveWidth(10)}
        });   
      
      this.setState({color1:'red',color2:"red"});
      
      }
}
facebookLogin = async () =>{
  try{
  const response = await Firebase.loginWithFacebook()
  console.log(response);
  if (response) {
    Toast.show({
      text:"Login Successfull",
      textStyle:{textAlign:"center"},
      duration:2000,
      style:{borderRadius:10,margin:responsiveWidth(10)}
    });
    this.storeItem("Name",response.facebookUserData.additionalUserInfo.profile.name);
    this.storeItem("Email",response.facebookUserData.additionalUserInfo.profile.email);
    this.storeItem("Photo",response.facebookUserData.additionalUserInfo.profile.picture.data.url);
    this.setState({loading:false});
    
    this.props.navigation.navigate('main');
  } }
  catch(error){
    Toast.show({
      text:"Something went wrong!",
      textStyle:{textAlign:"center"},
      duration:2000,
      style:{borderRadius:10,margin:responsiveWidth(10)}
    });
  }
}
googleLogin = async () =>{
  const response = await Firebase.loginWithGoogle()
  console.log(response);
  // if (response) {
   
  //   Toast.show({
  //     text:"Login Successfull",
  //     textStyle:{textAlign:"center"},
  //     duration:2000,
  //     style:{borderRadius:10,margin:responsiveWidth(10)}
  //   });
  //   this.storeItem("Name",response.facebookUserData.additionalUserInfo.profile.name);
  //   this.storeItem("Email",response.facebookUserData.additionalUserInfo.profile.email);
  //   this.storeItem("Photo",response.facebookUserData.additionalUserInfo.profile.picture.data.url);
  //   this.setState({loading:false});
    
  //   this.props.navigation.navigate('main');
  // } 
}
  storeItem = async (key, item) => {
    return new Promise(async (resolve, reject) => {
      try {
        var jsonOfItem = await AsyncStorage.setItem(key, JSON.stringify(item));
        resolve(jsonOfItem);
      } catch (error) {
        reject(error);
      }
    })
  } 

 
      render(){
        
        return (
          
         
          <SafeAreaView>
         
          <StatusBar barStyle = "light-content" hidden = {false} backgroundColor = "#000" translucent = {true}/>
         
            
          <ImageBackground source={require("../../../assets/bgnew.jpg")} style={{width:"100%", height:"100%"}}>
                  <Content>
                 
                  <View style={{marginTop:40, marginLeft:20}}>

                    <View style={{height:responsiveWidth(20),width:responsiveWidth(50),
                                  borderRadius:responsiveWidth(5), flexDirection:"row", alignItems:"center"}}>
                        <Image source={require("../../../assets/logo.png")} 
                              style={{height:responsiveWidth(20),width:responsiveWidth(20),
                                  borderRadius:responsiveWidth(5)}}/>
                        <Text style={{color:'#000046',fontSize:responsiveFontSize(4),fontWeight:"bold"}}>
                      NOMAD</Text>
                    </View>
                   
                    
                  </View>
                  <View style={{margin:responsiveWidth(5)}}>
                   
                    <Text style={{color:'#000',fontSize:responsiveFontSize(3), alignSelf:"center",marginTop:30}}>
                      Login to continue
                    </Text>
                    <Card style={{backgroundColor:"#f2f2f2",borderRadius:responsiveWidth(5)}}>
                      <CardItem style={{backgroundColor:"#f2f2f2",borderRadius:responsiveWidth(5)}}>
                        <View style={{flex:1,height:responsiveHeight(7)}}>
                          <Item style={{borderColor:this.state.color1}} floatingLabel>
                            <Label >Email</Label>
                            <Input value={this.state.email}
                                    onChangeText={(email) => this.setState({email})}
                                    autoCapitalize = "none"
                              />
                          </Item>
                        </View>
                        <View style={{alignItems:"flex-end",justifyContent:"flex-end",height:responsiveHeight(7)}}>
                            <Icon name="ios-mail" />
                        </View>
                      </CardItem>
                      <CardItem style={{backgroundColor:"#f2f2f2",borderRadius:responsiveWidth(5)}}>
                        <View style={{flex:1,height:responsiveHeight(7)}}>
                          <Item style={{borderColor:this.state.color2}} floatingLabel>
                            <Label >Password</Label>
                            <Input value={this.state.password}
                                    onChangeText={(password)=>this.setState({password})}
                                    secureTextEntry={this.state.passwordVisibility}
                              />
                          </Item>
                        </View>
                        <View style={{justifyContent:"flex-end",height:responsiveHeight(7)}}>
                          <TouchableOpacity onPress={this.handlePasswordVisibility}>
                            <Icon name={this.state.passwordShow}/>
                          </TouchableOpacity>
                        </View>
                      </CardItem>
                      <CardItem style={{backgroundColor:"#f2f2f2",borderRadius:responsiveWidth(5),justifyContent:"flex-end"}}>
                        <TouchableOpacity onPress={()=> this.props.navigation.navigate('forgot')}>
                          <Text style={{fontSize:responsiveFontSize(2)}}>
                            Forget Password?
                          </Text>
                        </TouchableOpacity>
                      </CardItem>
                      <CardItem style={{backgroundColor:"#f2f2f2",borderRadius:responsiveWidth(5),justifyContent:"center"}}>
                        <TouchableOpacity onPress={() => this.handleOnLogin()}
                            style={{alignItems:"center",justifyContent:"center",width:responsiveWidth(50)}}>
                          <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} 
                                          colors={[!this.state.loading?"#FF0000":"grey", !this.state.loading?"#6B0101":"grey"]}
                                          style={{borderRadius: 5,height:responsiveHeight(7),width:responsiveWidth(50),
                                                alignItems:"center",justifyContent:"center"}}>
                            <View >
                            <Text style={{color:'white'}}>
                              {this.state.loading?"Please Wait...":"Login"}</Text>
                            </View>
                          </LinearGradient>
                        </TouchableOpacity>
                       
                      </CardItem>
                      
                      <CardItem style={{backgroundColor:"#f2f2f2",borderRadius:responsiveWidth(5),justifyContent:"center"}}>
                        <Text style={{fontSize:responsiveFontSize(2)}}>
                        ---------------OR---------------
                        </Text>
                      </CardItem>
                      <CardItem style={{backgroundColor:"#f2f2f2",borderRadius:responsiveWidth(5),justifyContent:"center"}}>
                       
                        <TouchableOpacity onPress={() => this.facebookLogin()}
                                        style={{alignItems:"center",justifyContent:"center"}}> 
                                <SocialIcon type="facebook"/>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.googleLogin()}
                                        style={{alignItems:"center",justifyContent:"center"}}> 
                                <SocialIcon type="google" />
                        </TouchableOpacity>
                        
                      </CardItem>
                      <CardItem style={{backgroundColor:"#f2f2f2",borderRadius:responsiveWidth(5),justifyContent:"center"}}>
                        <TouchableOpacity onPress={()=> this.props.navigation.navigate('signup')}>
                          <Text style={{fontSize:responsiveFontSize(2)}}>
                             Don't have an account? Sign Up
                          </Text>
                        </TouchableOpacity>
                        
                      </CardItem>
                      
                      
                      
                    </Card>
                    
                  </View>
                  
                  </Content>
                  {this.state.showLoader && <Loader
                                                  onRef = {ref => this.Modal = ref}
                                                            
                                                  onAction = {() => this.setval()}/>}
                
                
           
                </ImageBackground>
           
        
                </SafeAreaView>
          
        );
      }
    }

   
 

