import React from 'react';
import {Text, View,TouchableOpacity,AsyncStorage,ScrollView,SafeAreaView,StatusBar,TouchableWithoutFeedback,Keyboard,KeyboardAvoidingView,ImageBackground,Image} from 'react-native';
import {SocialIcon,CheckBox,Button,} from 'react-native-elements';
import {Container,Body,Icon,Input, Content,Item, Toast,Label,Card,CardItem,Spinner} from "native-base";
import {responsiveWidth,responsiveHeight,responsiveFontSize} from "react-native-responsive-dimensions";
import { LinearGradient } from 'expo-linear-gradient';
import Firebase from "../../../config/firebase/firebase";

export default class SignUp extends React.Component {
    constructor(props){
      super(props);
      this.state = {
                    username: '',
                    email:'',
                    password: '',
                    conPassword:'',
                    color:"grey",
                    color1:"grey",
                    color2:"grey",
                    color3:"grey",
                    loading: false,
                    passwordVisibility: true,
                    confirmPasswordVisibility:true,
                    passwordShow:'ios-eye',
                    conPasswordShow:'ios-eye',
                    check:true
                  };
      }
  handlePasswordVisibility = () => {
    this.setState({
      passwordShow: this.state.passwordShow === 'ios-eye' ? 'ios-eye-off' : 'ios-eye',
      passwordVisibility: !this.state.passwordVisibility
    })
  }
  handleConfirmPasswordVisibility = () => {
    this.setState({
      conPasswordShow: this.state.conPasswordShow === 'ios-eye' ? 'ios-eye-off' : 'ios-eye',
      confirmPasswordVisibility: !this.state.confirmPasswordVisibility
    })
  }
  validateEmail = ()=>{
    var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    if(emailPattern.test(this.state.email)){
        return true;
      }
    return false;
  }
  validatePass = ()=>{
    var passPattern = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*_-])[A-Za-z\d!@#$%^&*_-]{8,}$/;
    if(passPattern.test(this.state.password)){
        return true;
      }
    return false;
  }
  handleOnSingup = async () => {
    const { username,email,password,conPassword } = this.state
    if( username == "" || email == "" || password == "" || conPassword == "" )
      {
        this.setState({color:"red",color1:"red",color2:'red',color3:"red",loading:false})
        Toast.show({
              text:"Please fill the required fields",
              textStyle:{textAlign:"center"},
              duration:2000,
              style:{borderRadius:10,margin:responsiveWidth(10)}
            });
      }
    else
      {
        if(!this.validateEmail())
          {
            this.setState({color:"green",color1:'red',loading:false})
            Toast.show({
                  text:"please fill email in correct format (abc@email.com)",
                  textStyle:{textAlign:"center"},
                  duration:2000,
                  style:{borderRadius:10,margin:responsiveWidth(10)}
                });
          }
        else if(!this.validatePass())
          {
            this.setState({color:"green",color1:"green",color2:'red',loading:false})
            Toast.show({
              text:"Password must include minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character",
              textStyle:{textAlign:"center"},
              duration:2000,
              style:{borderRadius:10,margin:responsiveWidth(10)}
            }); 
          }
        else if(password !== conPassword)
          {
            this.setState({color:"green",color1:"green",color2:'red',color3:"red",loading:false})
            Toast.show({
              text:"Password must include be same",
              textStyle:{textAlign:"center"},
              duration:2000,
              style:{borderRadius:10,margin:responsiveWidth(10)}
            }); 
          }
        else
          { this.setState({color:"grey",color1:"grey",color2:"grey",color:"grey",loading:true})
            try {
                  const response = await Firebase.signupWithEmail(email,password)
                 // console.log(response);
                  this.storeItem("Name",username);
                  this.storeItem("Email",email);
                  this.setState({loading:false})
                  this.props.navigation.navigate("login")
                } 
            catch (error) {this.setState({loading:false})}
          }
      }
  }
storeItem = async (key,Item) => {
    return new Promise(async (resolve, reject) => {
      try {
        var jsonOfItem = await AsyncStorage.setItem(key, JSON.stringify(Item));
        resolve(jsonOfItem);
      } catch (error) {
        reject(error);
      }
    })
  } 
render(){
  return (
          
         <SafeAreaView>
    <StatusBar barStyle = "light-content" hidden = {false} backgroundColor = "#000" translucent = {true}/>
     <ScrollView contentContainerStyle> 
              <Container>
                <ImageBackground source={require("../../../assets/bgnew.jpg")} resizeMode="stretch" style={{width:responsiveWidth(100),
                                height:responsiveHeight(100)}} blurRadius={1}>
                                 

                                        
                <Content>
                
                    <View style={{height:responsiveWidth(20),width:responsiveWidth(50),marginTop:40,marginLeft:20,
                                  borderRadius:responsiveWidth(5), alignItems:"center", flexDirection:"row"}}>
                        <Image source={require("../../../assets/logo.png")} 
                              style={{height:responsiveWidth(20),width:responsiveWidth(20),
                                  borderRadius:responsiveWidth(5)}}/>
                          <Text style={{color:'#000046',fontSize:responsiveFontSize(4),fontWeight:"bold"}}>
                      NOMAD</Text>
                    </View>
                   
                    
                
                  <View style={{margin:responsiveWidth(5)}}>
                   
                    <Text style={{color:'#000',fontSize:responsiveFontSize(3), alignSelf:"center", marginTop:30}}>
                      Sign Up to continue
                    </Text>
                    <Card style={{backgroundColor:"#f2f2f2",borderRadius:responsiveWidth(5)}}>
                      <CardItem style={{backgroundColor:"#f2f2f2",borderRadius:responsiveWidth(5)}}>
                        <View style={{flex:1,height:responsiveHeight(7)}}>
                          <Item style={{borderColor:this.state.color}} floatingLabel>
                            <Label >Username</Label>
                            <Input value={this.state.username}
                                    onChangeText={(username) => this.setState({username})}
                                    autoCapitalize = "none"
                              />
                          </Item>
                        </View>
                        <View style={{alignItems:"flex-end",justifyContent:"flex-end",height:responsiveHeight(7)}}>
                            <Icon name="user" type="Entypo" />
                        </View>
                      </CardItem>
                      <CardItem style={{backgroundColor:"#f2f2f2",borderRadius:responsiveWidth(5)}}>
                        <View style={{flex:1,height:responsiveHeight(7)}}>
                          <Item style={{borderColor:this.state.color1}} floatingLabel>
                            <Label >Email</Label>
                            <Input value={this.state.email}
                                    onChangeText={(email) => this.setState({email})}
                                    autoCapitalize = "none"
                              />
                          </Item>
                        </View>
                        <View style={{alignItems:"flex-end",justifyContent:"flex-end",height:responsiveHeight(7)}}>
                            <Icon name="ios-mail"/>
                        </View>
                      </CardItem>
                      <CardItem style={{backgroundColor:"#f2f2f2",borderRadius:responsiveWidth(5)}}>
                        <View style={{flex:1,height:responsiveHeight(7)}}>
                          <Item style={{borderColor:this.state.color2}} floatingLabel>
                            <Label >Password</Label>
                            <Input value={this.state.password}
                                    onChangeText={(password)=>this.setState({password})}
                                    secureTextEntry={this.state.passwordVisibility}
                              />
                          </Item>
                        </View>
                        <View style={{justifyContent:"flex-end",height:responsiveHeight(7)}}>
                          <TouchableOpacity onPress={this.handlePasswordVisibility}>
                            <Icon name={this.state.passwordShow}/>
                          </TouchableOpacity>
                        </View>
                      </CardItem>
                      <CardItem style={{backgroundColor:"#f2f2f2",borderRadius:responsiveWidth(5)}}>
                        <View style={{flex:1,height:responsiveHeight(7)}}>
                          <Item style={{borderColor:this.state.color3}} floatingLabel>
                            <Label >Confirm Password</Label>
                            <Input value={this.state.conPassword}
                                    onChangeText={(conPassword)=>this.setState({conPassword})}
                                    secureTextEntry={this.state.confirmPasswordVisibility}
                              />
                          </Item>
                        </View>
                        <View style={{justifyContent:"flex-end",height:responsiveHeight(7)}}>
                          <TouchableOpacity onPress={this.handleConfirmPasswordVisibility}>
                            <Icon name={this.state.conPasswordShow}/>
                          </TouchableOpacity>
                        </View>
                      </CardItem>
                       <CheckBox
                        containerStyle={{backgroundColor: '#f2f2f2',
                        borderColor: '#f2f2f2',}}
                        checkedIcon='check-box'
                        iconType='material'
                        checkedColor="red"
                        uncheckedIcon='check-box-outline-blank'
                        title='Agree to terms and conditions'
                        checkedTitle='You agreed to our terms and conditions'
                        checked={this.state.check}
                        onPress={() => this.setState({'check':!this.state.check})}
                      />
                      <CardItem style={{backgroundColor:"#f2f2f2",borderRadius:responsiveWidth(5),justifyContent:"center"}}>
                        <TouchableOpacity onPress={() => this.handleOnSingup()}
                            style={{alignItems:"center"}}>
                          <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={[!this.state.loading?"#FF0000":"grey", !this.state.loading?"#6B0101":"grey"]}
                                          style={{borderRadius: 5,height:responsiveHeight(7),width:responsiveWidth(50)}}>
                            <View >
                            <Text style={{color:'white',textAlign:"center",marginTop:responsiveHeight(2)}}>
                              {this.state.loading?"Please Wait...":"Sign Up"}</Text>
                            </View>
                          </LinearGradient>
                        </TouchableOpacity>
                      </CardItem>
                      <CardItem style={{backgroundColor:"#f2f2f2",borderRadius:responsiveWidth(5),justifyContent:"center"}}>
                        <TouchableOpacity onPress={()=> this.props.navigation.navigate('login')}>
                          <Text style={{fontSize:responsiveFontSize(2)}}>
                             Already have an account? Sign In
                          </Text>
                        </TouchableOpacity>
                      </CardItem>
                    </Card> 
                  </View>  
                 
                  </Content>
                
                 
                </ImageBackground>
                </Container>
                </ScrollView>
                 
                </SafeAreaView>
        
          
        );
      }
    }

   
 

