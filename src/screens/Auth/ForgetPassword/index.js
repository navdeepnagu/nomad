import React from "react";
import {View,Text,TouchableOpacity,Image, ImageBackground,TextInput,SafeAreaView,StatusBar, Alert} from "react-native";
import {Container, Header, Content, Card, CardItem, Body,Input, Button,Toast, Icon} from "native-base";
import {responsiveHeight,responsiveWidth,responsiveFontSize} from "react-native-responsive-dimensions";
import { LinearGradient } from "expo-linear-gradient";
import HeaderCOM from '../../../components/Header';
import Firebase from "../../../config/firebase/firebase";



class Forgot extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      email : "",
      loading : false,
      color:"#000"
    };
  }

  validateEmail = ()=>{
    var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    if(emailPattern.test(this.state.email)){
        return true;
    }
    return false;
  }
  
  resetPassword = () =>{
    let email = this.state.email;
    if(!this.validateEmail()){
      this.setState({color:"red"});
      Toast.show({
        text:"please fill email in correct format (abc@email.com)",
        textStyle:{textAlign:"center"},
        duration:2000,
        style:{borderRadius:10,margin:responsiveWidth(10)}
      });
      return false;
    }
    this.setState({loading:true});
    Firebase.passwordReset(email)
    .then((response)=>{
      this.setState({color:"green"});
      Toast.show({
        text:response.message,
        textStyle:{textAlign:"center"},
        duration:2000,
        style:{borderRadius:10,margin:responsiveWidth(10)}
      });
      setTimeout(()=>{
        this.setState({loading:false})
      },2000);
    })
    .catch((err)=>{
      this.setState({loading:false})
      Toast.show({
        text:err.message,
        textStyle:{textAlign:"center"},
        duration:2000,
        style:{borderRadius:10,margin:responsiveWidth(10)}
      });
    })
  } 
    render(){
        return(
          <SafeAreaView style={{flex:1}}>
            <StatusBar barStyle = "light-content" hidden = {false} backgroundColor = "#000" translucent = {true}/>
            <Container>
            <ImageBackground source={require("../../../assets/bgnew.jpg")} style={{width:"100%", height:"100%"}}>
              <View style={{flex:0.1, backgroundColor:"#3f3f3f", flexDirection:"row", marginTop:20, alignItems:"center"}}>
                <TouchableOpacity onPress={()=>{this.props.navigation.goBack()}}>
                  <Icon name="arrowleft" type="AntDesign" style={{color:"white", marginLeft:10}}/>
                </TouchableOpacity>
                <Text style={{color:"white", fontSize:responsiveFontSize(3), marginLeft:10}}>Forgot password?</Text>
              </View>
         {/* <HeaderCOM {...this.props} name="Truoble signing in ?" btnValue={true} btnTitle="arrow-back" 
                    onAction = {()=>this.props.navigation.goBack()}/> */}
        <Content padder>
          <Card>
            <CardItem header bordered style={{backgroundColor:"#6B0101",height:responsiveHeight(7)}}>
              <Text style={{fontWeight:"bold",fontSize:responsiveFontSize(2.5), color:"white"}}>Forgotten email ?</Text>
            </CardItem>
            <CardItem bordered>
              <Body style={{alignItems:"center",justifyContent:"center"}}>
              
                <Text style={{fontSize:responsiveFontSize(2.5)}}>Please enter the email you signed up with.We will send you a link to change your password.</Text>
                <Input  style={{borderWidth:1,width:responsiveWidth(80),height:responsiveHeight(6),
                                margin:responsiveHeight(5),borderRadius:responsiveWidth(2),borderColor:this.state.color}}
                        onChangeText={(email) => this.setState({email})}
                        placeholder='Enter your Registered Email'>

                </Input>

              </Body>
            </CardItem>
            <CardItem footer bordered>
            <Body style={{alignItems:"center",justifyContent:"center"}}>
            <TouchableOpacity onPress={() => this.resetPassword()} >
                  <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={[!this.state.loading?"#FF0000":"grey", !this.state.loading?"#6B0101":"grey"]}
                                  style={{borderRadius:responsiveWidth(5),height:responsiveHeight(7),
                                        width:responsiveWidth(60),margin:responsiveWidth(2)}}>
                    <View >
                    <Text style={{color:'white',textAlign:"center",marginTop:responsiveHeight(2)}}>
                    {this.state.loading?"Please Wait...":"Send Password Reset Link"}
                    </Text>
                    </View>
                  </LinearGradient>
                </TouchableOpacity>
                </Body>
           </CardItem>
          </Card>
        </Content>
        </ImageBackground>
      </Container>
      </SafeAreaView>
    );
  }
}


export default Forgot;