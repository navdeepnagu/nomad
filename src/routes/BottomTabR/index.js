import React from 'react';
import {View,Icon,Text} from "native-base";
import { createBottomTabNavigator,createMaterialTopTabNavigator } from 'react-navigation-tabs';
import {responsiveHeight,responsiveFontSize,responsiveWidth} from 'react-native-responsive-dimensions';
import {createAppContainer} from "react-navigation";
import HOMESCREEN from "../../screens/Main/BottonTabPage/HomeScreen";
import {Chat,FindRide,OfferRide,Profile} from "../../screens/Main/BottonTabPage";

const BottomTabContainer = createMaterialTopTabNavigator({
    homescreen:{screen:HOMESCREEN,
        navigationOptions:{
            tabBarLabel: ({ tintColor, focused }) => (
              <Icon
              name={focused ? 'home' : 'home'}
              style={{ color: focused ? '#000046' : tintColor}}
              />
            ),
          },
          },
   
   findride:{screen:FindRide,
    navigationOptions:{
        tabBarLabel: ({ tintColor, focused }) => (
          <Icon
          name={focused ? 'search' : 'search'}
          style={{ color: focused ? '#000046' : tintColor}}
          />
        ),
      },
      
               
},
   offerride:{screen:OfferRide,
    navigationOptions:{
        tabBarLabel: ({ tintColor, focused }) => (
          <Icon
          name={focused ? 'plus-circle-outline' : 'plus-circle-outline'}
          type="MaterialCommunityIcons"
          style={{ color: focused ? '#000046' : tintColor}}
          />
        ),
      },
      },
    chat:{screen:Chat,
        navigationOptions:{
            tabBarLabel: ({ tintColor, focused }) => (
              <Icon
              name={focused ? 'wechat' : 'wechat'}
              type="MaterialCommunityIcons"
              style={{ color: focused ? '#000046' : tintColor}}
              />
            ),
          },
          },
   profile:{screen:Profile,
    navigationOptions:{
        tabBarLabel: ({ tintColor, focused }) => (
          <Icon
          name={focused ? 'account-circle' : 'account-circle'}
          type="MaterialCommunityIcons"
          style={{ color: focused ? '#000046' : tintColor}}
          />
        ),
      },
      },
},{
   
    tabBarPosition:"bottom",
    tabBarOptions:{
        style:{backgroundColor:"#f11" },
       
          
    },
    
    
    
    
    
    
});

export default createAppContainer(BottomTabContainer);