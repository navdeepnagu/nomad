import React from 'react';
import {View,Icon,Text,Thumbnail} from "native-base";
import {responsiveWidth,responsiveHeight,responsiveFontSize} from "react-native-responsive-dimensions";
import {createDrawerNavigator} from "react-navigation-drawer";
import {createAppContainer, createSwitchNavigator} from "react-navigation";
import Drawer from "../../components/Drawer";
import Home from '../../screens/Main/Homepage';
import {About,History,Payment,ReferEarn,Setting,Support} from '../../screens/Main/DrawerPage';

const DrawerContainer = createDrawerNavigator({
    home:{screen:Home,
        navigationOptions:{
           drawerLabel:({ tintColor, focused }) => 
                        <Text  style={{fontWeight: focused ? 'bold' : "normal",fontSize:responsiveFontSize(2.5)}}>
                            Home
                        </Text>,
           drawerIcon:() => <Thumbnail square source={require("../../assets/icon/home.png")} 
                                    style={{height:responsiveWidth(10),width:responsiveWidth(10),margin:responsiveWidth(3)}}/>,
          },
        },
    history:{screen:History,
        navigationOptions:{
           drawerLabel:({ tintColor, focused }) => 
                        <Text  style={{fontWeight: focused ? 'bold' : "normal",fontSize:responsiveFontSize(2.5)}}>
                            My Rides
                        </Text>,
           drawerIcon:() => <Thumbnail square source={require("../../assets/icon/history.png")} 
                                    style={{height:responsiveWidth(10),width:responsiveWidth(10),margin:responsiveWidth(3)}}/>,
          },
        },
    payment:{screen:Payment,
        navigationOptions:{
           drawerLabel:({ focused }) => 
                        <Text  style={{fontWeight: focused ? 'bold' : "normal",fontSize:responsiveFontSize(2.5)}}>
                            Payments
                        </Text>,
           drawerIcon:() => <Thumbnail square source={require("../../assets/icon/wallet.png")} 
                                    style={{height:responsiveWidth(8),width:responsiveWidth(8),margin:responsiveWidth(3)}}/>,
          },
        },
    referearn:{screen:ReferEarn,
        navigationOptions:{
           drawerLabel:({ focused }) => 
                        <Text  style={{fontWeight: focused ? 'bold' : "normal",fontSize:responsiveFontSize(2.5)}}>
                            Refer and Earn
                        </Text>,
           drawerIcon:() => <Thumbnail square source={require("../../assets/icon/gift.png")} 
                                    style={{height:responsiveWidth(8),width:responsiveWidth(8),margin:responsiveWidth(3)}}/> ,
          },
        },
    setting:{screen:Setting,
        navigationOptions:{
           drawerLabel:({ focused }) => 
                        <Text style={{fontWeight: focused ? 'bold' : "normal",fontSize:responsiveFontSize(2.5)}}>
                            Settings
                        </Text>,
           drawerIcon:() => <Thumbnail square source={require("../../assets/icon/settings.png")} 
                                    style={{height:responsiveWidth(8),width:responsiveWidth(8),margin:responsiveWidth(3)}}/>,
          },
        },
    about:{screen:About,
        navigationOptions:{
           drawerLabel:({ focused }) => 
                        <Text style={{fontWeight: focused ? 'bold' : "normal",fontSize:responsiveFontSize(2.5)}}>
                            About
                        </Text>,
           drawerIcon:() => <Thumbnail source={require("../../assets/icon/about.jpg")} 
                                    style={{height:responsiveWidth(8),width:responsiveWidth(8),margin:responsiveWidth(3)}}/> ,
          } ,
          
        },
    support:{screen:Support,
        navigationOptions:{
           drawerLabel:({ focused }) => 
                        <Text style={{fontWeight: focused ? 'bold' : "normal",fontSize:responsiveFontSize(2.5)}}>
                            Help
                        </Text>,
           drawerIcon:() => <Thumbnail source={require("../../assets/icon/help.png")} 
                                    style={{height:responsiveWidth(8),width:responsiveWidth(8),margin:responsiveWidth(3)}}/> ,
          } ,
          
        },
   
},{
    initialRouteName : "home",
    hideStatusBar:true,
    contentComponent: props => <Drawer {...props} />,
    drawerOpenRoute:"DrawerOpen",
    drawerWidth:'80%',
    
    
});

export default createAppContainer(DrawerContainer);