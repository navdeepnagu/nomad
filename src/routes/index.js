import {createStackNavigator} from "react-navigation-stack";
import {createAppContainer,createSwitchNavigator} from "react-navigation";
import Auth from "./AuthR";
import Main from "./MainR";
import Splash from "../screens/SplashScreen";

const appContainer = createSwitchNavigator({
    auth:Auth,
    main:Main,
    splash:Splash,
   
},{
    initialRouteName:'splash',
    headerMode:null
});

export default createAppContainer(appContainer);