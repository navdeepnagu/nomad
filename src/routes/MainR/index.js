import {createStackNavigator} from "react-navigation-stack";
import {createAppContainer} from "react-navigation";
import DrawerRoutes from "../DrawerR";

const mainContainer = createStackNavigator({
    drawerRoutes:DrawerRoutes
    
   
},{
    initialRouteName:'drawerRoutes',
    headerMode:null
});

export default createAppContainer(mainContainer);