import { createBottomTabNavigator,createMaterialTopTabNavigator } from 'react-navigation-tabs';
import {createAppContainer} from "react-navigation";
import {createStackNavigator} from 'react-navigation-stack';
import {Login,Signup,Forgot } from "../../screens/Auth";

const Authpage = createStackNavigator({
       login:Login,
       signup:Signup,
       forgot:Forgot
       
    },{
        initialRouteName:'login',
        headerMode:null
    });
    
    export default createAppContainer(Authpage);