import * as firebase from 'firebase';
import firebaseConfig from './config';
import * as Facebook from 'expo-facebook';
import * as Google from 'expo-google-app-auth';


if (firebase.apps.length === 0) {
    firebase.initializeApp(firebaseConfig);
}
else{
    firebase.app();
}   

const Firebase = {
    loginWithEmail: (email, password) => {
        let response = {}
        return new Promise(async(resolve,reject)=>{
            try{
                let user_data = await firebase.auth().signInWithEmailAndPassword(email, password)
                response.status = true;
                response.message = "Login Success";
                response.data = user_data;
                resolve(response)
            }
            catch(err){
                response.status = false;
                response.message = err.message;
                response.data = undefined;
                resolve(response)
            }
        })
    },
    
    signupWithEmail: (email, password) => {
        let response = {}
        return new Promise(async(resolve,reject)=>{
            try{
                let user_data = await firebase.auth().createUserWithEmailAndPassword(email, password)
                response.status = true;
                response.message = "Account Created Successfully";
                response.data = user_data;
                resolve(response)
            }
            catch(err){
                response.status = false;
                response.message = err.message;
                response.data = undefined;
                resolve(err) 
            }
        })
    },
    passwordReset : (email) => {
        let response = {}
        return new Promise(async(resolve,reject)=>{
            try{
                let result = await firebase.auth().sendPasswordResetEmail(email)
                response.status = true;
                response.message = "Password Reset Link has been sent to your Registered Email Address.";
                response.data = result;
                resolve(response) 
            }
            catch(err){
                response.status = true;
                response.message = err.message;
                response.data = result;
                reject(response)
            }
        })
    }, 
    loginWithFacebook : async () => {
       try{ 
           await Facebook.initializeAsync('223957445346702');
            const permissions = ['public_profile', 'email'];
            const {type,token} = await Facebook.logInWithReadPermissionsAsync({permissions});
            switch (type) {
                    case 'success': {
                                    await firebase.auth().setPersistence(firebase.auth.Auth.Persistence.LOCAL); 
                                    const credential = firebase.auth.FacebookAuthProvider.credential(token);
                                    const facebookUserData = await firebase.auth().signInWithCredential(credential);   
                                    return Promise.resolve({type: 'success',facebookUserData});
                                }
                    case 'cancel': {
                                     return Promise.reject({type: 'cancel'});
                                 }
                        }
        }
       catch(error){
                     return { error };
        }
      },
    loginWithGoogle : async () => {
        try{ 
            
                const permissions = {
                                    expoClientId: '372959820165-l7356s4qgvb2l4demljdea06hvg93bof.apps.googleusercontent.com',
                                    androidClientId: '372959820165-a5i2u3ees3ub409ibkn22bkfsmciniv4.apps.googleusercontent.com',
                                    iosClientId: '372959820165-vte712dl6d1li5iajqiekl8c35dk0rnv.apps.googleusercontent.com',
                                    scopes: ['profile', 'email']
                                };
                const {type,token} = await Google.logInAsync({permissions});
                switch (type) {
                        case 'success': {
                                        await firebase.auth().setPersistence(firebase.auth.Auth.Persistence.LOCAL); 
                                        const credential = firebase.auth.GoogleAuthProvider.credential(token);
                                        const googleUserData = await firebase.auth().signInWithCredential(credential);   
                                        return Promise.resolve({type: 'success',googleUserData});
                                    }
                        case 'cancel': {
                                        return Promise.reject({type: 'cancel'});
                                    }
                            }
            }
        catch(error){
                        return { error };
            }
    },
    userSignOut :  () => {
            let response = {}
            return new Promise(async(resolve,reject)=>{
                try{
                    let result = await firebase.auth().signOut()
                    response.status = true;
                    response.message = "logout success";
                    resolve(response) 
                }
                catch(err){
                    response.status = false;
                    response.message = err.message;
                    reject(response)
                }
            })
            
    },
      
      

}

export default Firebase