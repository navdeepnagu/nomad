import React , {Component} from "react";
import {Text, View, StyleSheet} from "react-native";
import { Container, Header, Content, Card, CardItem, Body, Input,Item,Icon,Label,Button,Fab} from "native-base";

import Modal from "react-native-modalbox";
import { responsiveHeight, responsiveWidth, responsiveFontSize } from "react-native-responsive-dimensions";
import { TouchableOpacity } from "react-native-gesture-handler";

export default class MyModal extends Component{
    constructor(props){
        super(props);
        this.state = {
            isOpen: true,
            seacrhLoc:""
        }
    }
    componentDidMount(){
        this.props.onRef(this.refs.Modal); 
    }

    render(){
        return (
            <Modal 
                style={styles.modal}
                isOpen={this.state.isOpen}
                ref = {"Modal"}
                swipeToClose={true}
                >
                <Container>
                    <Content >
                        <CardItem>       
                            <Item rounded style={{backgroundColor:"#d9d9d9"}} >
                                <TouchableOpacity onPress={()=>this.refs.Modal.close()}>
                                    <Icon name='ios-arrow-back'  style={{color:"grey"}} />
                                </TouchableOpacity>
                                <Input placeholder="e.g. Sector-17, Chandigarh"
                                        placeholderTextColor="grey"
                                        value={this.state.seacrhLoc}
                                        onChangeText={(seacrhLoc) => this.setState({seacrhLoc})}
                                        style={{height:responsiveHeight(7)}}/>
                            </Item>    
                        </CardItem>
                        <CardItem >       
                           <TouchableOpacity onPress={()=>{}} style={{flexDirection:"row",marginLeft:responsiveWidth(5)}}>
                                <Icon name='md-locate' type="Ionicons"  />
                                <Text style={{fontSize:responsiveFontSize(2.5)}}>Use current location</Text>
                            </TouchableOpacity>
                        </CardItem>    
                        
                        
                        <View style={{alignItems:"flex-end",justifyContent:"flex-end",marginTop:responsiveHeight(60),
                                        marginRight:responsiveWidth(5)}}>
                                <TouchableOpacity  onPress={()=>this.props.onAction(this.state.seacrhLoc,this.state.isOpen)} 
                                                    style={{height:responsiveHeight(10),width:responsiveWidth(20),
                                                            backgroundColor:"green",borderRadius:responsiveWidth(50), 
                                                            alignItems:"center",justifyContent:"center"}}>
                                    <Icon name='ios-arrow-forward'   style={{color:"#fff"}} />
                                </TouchableOpacity>
                               
                        </View>      
                </Content>
                </Container>
            </Modal>
           
        );
    }
}

const styles = StyleSheet.create({
    container:{
        justifyContent:"center",
        flex:1,
        alignItems:"center",
        backgroundColor:"red"
    },
    modal: {
        backgroundColor:"#f2f2f2",
        height:responsiveHeight(100),
        width:responsiveWidth(100)
    },
    btn: {
        margin: 10,
        backgroundColor: "#3B5998",
        color: "white",
        padding: 10
    },
    btnModal: {
        position: "absolute",
        top: 0,
        right: 0,
        width: 50,
        height: 50,
        backgroundColor: "transparent"
    },
    
    text: {
        color: "black",
        fontSize: 22
    }
});