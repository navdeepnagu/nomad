import React , {Component} from "react";

import {Text, View, StyleSheet, Button} from "react-native";

import Modal from "react-native-modalbox";
import { responsiveHeight, responsiveWidth } from "react-native-responsive-dimensions";
import { List, ListItem } from "native-base";

export default class OptionModal extends Component{
    constructor(props){
        super(props);
        this.state = {
            isOpen: false
        }
    }
    componentDidMount(){
        this.props.onRef(this.refs.Modal); 
    }

    render(){
        return (
            <Modal
                style={{height:100,width:responsiveWidth(40),marginLeft:responsiveWidth(29),marginTop:responsiveHeight(1),
                        backgroundColor:"#cccccc"}}
                isOpen={this.state.isOpen}
                ref = {"Modal"}
                backdrop={true}
                backButtonClose={true}
                swipeToClose={false}
                position="top"
                backdropOpacity={0.2}
                entry="top"
                >
            <List>
                <ListItem noBorder
                    button
                    onPress={() => this.props.onAction()}>
                    <Text >
                        Settings
                    </Text>
                  </ListItem>
                <ListItem noBorder
                    button
                    onPress={() => this.props.onAction()}>
                    <Text >
                        Help
                    </Text>
                </ListItem>
            </List>   
              
            </Modal>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        justifyContent:"center",
        flex:1,
        alignItems:"center",
        backgroundColor:"red"
    },
    modal: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    btn: {
        margin: 10,
        backgroundColor: "#3B5998",
        color: "white",
        padding: 10
    },
    btnModal: {
        position: "absolute",
        top: 0,
        right: 0,
        width: 50,
        height: 50,
        backgroundColor: "transparent"
    },
    
    text: {
        color: "black",
        fontSize: 22
    }
});