import React from 'react';
import {DrawerItems} from "react-navigation-drawer";
import { StyleSheet, View,TouchableOpacity, Image,ImageBackground ,AsyncStorage} from 'react-native';
import {Container, Content, Text, List, ListItem ,Icon, Button, Right,Thumbnail} from "native-base";
import {responsiveWidth,responsiveHeight,responsiveFontSize} from "react-native-responsive-dimensions";
import { LinearGradient } from 'expo-linear-gradient';

export default class Drawer extends React.Component {
    constructor(props){
        super(props);
        this.state={
          name:"",
          email:"",
          photo:""
        }

    }
   
    getdata=async()=>{
     var  user_name=await AsyncStorage.getItem('Name');
     const name = JSON.parse(user_name);
     var  user_email=await AsyncStorage.getItem('Email');
     const mail = JSON.parse(user_email);
      var user_photo=await AsyncStorage.getItem('Photo');
      const uPhoto = JSON.parse(user_photo);
     this.setState({name:name,email:mail,photo:uPhoto});
    }
  
  render() {
    this.getdata();
    return (
      <Container>
        <Content>
        
              <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={['#FF0000', '#6B0101']}>
                <View style={{margin:responsiveHeight(2),marginTop:responsiveHeight(5),marginBottom:responsiveHeight(5),
                        flexDirection:"row",alignItems:"center"}}  >
                      <Image  //source={{uri:this.state.photo}}
                              style={{width:responsiveWidth(18),height:responsiveWidth(18),borderRadius:responsiveWidth(10)}} />
                  
                  <View style={{marginLeft:responsiveWidth(5)}}>
                    <Text style={{color:"#fff",fontSize:responsiveFontSize(2.5),fontWeight:"bold"}}>{this.state.name}</Text>
                    <Text style={{color:"#fff",fontSize:responsiveFontSize(2)}}>{this.state.email}</Text>
                    </View>
                  </View>
                </LinearGradient>
            
                <Content>
                  <DrawerItems {...this.props}/>
                </Content>
           
                </Content>
            <View style={{height:responsiveHeight(5),backgroundColor:"#d9d9d9",alignItems:"center",justifyContent:"center"}}>
                <Text style={style.textstyle}>
                    version:1.0.0
                </Text>
            </View>
            
        
      </Container>
    );
  }
}
const style = StyleSheet.create({
    Container:{
               flex: 1,
            },
            textstyle:{
              fontSize:responsiveFontSize(2),
              marginLeft:responsiveWidth(5)
            
            
            
            }
            

});